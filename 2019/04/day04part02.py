#!/usr/bin/env python

import argparse
import re

def is_valid(p):

    prev = 0
    for c in map(int, str(p)):
        if c < prev:
            return False
        prev = c

    # if not re.findall(r'^(?!.([0-9])\1{2})[0-9]+$', str(p)):
    #     return False
    if not re.findall(r'(\d)\1+', str(p)):
        return False

    return True

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    args = parser.parse_args()

    count = 0
    total = 0
    valid_list = []

    with open(args.input) as file:
        password_ranges = file.read().split()[0].split('-')

        for x in range(int(password_ranges[0]), int(password_ranges[1]) + 1):
            total = total + 1

            if is_valid(x):
                valid_list.append(x)
                count = count + 1

    final_list = []

    for val in valid_list:
        char_count = {}
        for c in str(val):
            if c in char_count:
                char_count[c] += 1
            else:
                char_count[c] = 1
        print(val)
        print(char_count)

        valid = False
        for c in char_count:
            if char_count[c] == 2:
                valid = True
            # if char_count[c] % 2 == 1 and char_count[c] != 1:
            #     valid = False

        if valid:
            final_list.append(val)

    print(len(final_list))
    print(final_list)

