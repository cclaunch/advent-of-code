#!/usr/bin/env python

import argparse
import re

def is_valid(p):

    prev = 0
    for c in map(int, str(p)):
        if c < prev:
            return False
        prev = c

    if not re.findall(r'(\d)\1+', str(p)):
        return False

    return True

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    args = parser.parse_args()

    count = 0
    total = 0

    with open(args.input) as file:
        password_ranges = file.read().split()[0].split('-')

        for x in range(int(password_ranges[0]), int(password_ranges[1]) + 1):
            total = total + 1

            if is_valid(x):
                count = count + 1

    print(count)

