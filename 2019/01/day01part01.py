#!/usr/bin/env python
import argparse

def calc_fuel(input_file):

    total = 0

    # open the file, add up all the lines, easy peasy
    with open(input_file) as file:
        for mass in file:
            total = total + int(int(mass) / 3) - 2

    return total

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="puzzle input")
    args = parser.parse_args()

    print(calc_fuel(args.input))
