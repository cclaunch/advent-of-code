#!/usr/bin/env python
import argparse

def calc_fuel(input_file):

    total = 0

    # open the file, add up all the lines, easy peasy
    with open(input_file) as file:
        for mass in file:
            fuel_mass = int(mass)
            while fuel_mass > 0:
                fuel_mass = int(fuel_mass / 3) - 2
                if fuel_mass > 0:
                    total = total + fuel_mass

    return total

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="puzzle input")
    args = parser.parse_args()

    print(calc_fuel(args.input))
