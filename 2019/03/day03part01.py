#!/usr/bin/env python

import argparse
from scipy.spatial import distance

def draw_wire(wire):

    x = 0
    y = 0
    wire_set = set()

    for step in wire:
        if step[0] == 'R':
            for i in range(step[1]):
                wire_set.add((x, y))
                x = x + 1
        if step[0] == 'L':
            for i in range(step[1]):
                wire_set.add((x, y))
                x = x - 1
        if step[0] == 'U':
            for i in range(step[1]):
                wire_set.add((x, y))
                y = y + 1
        if step[0] == 'D':
            for i in range(step[1]):
                wire_set.add((x, y))
                y = y - 1

    return wire_set


def parse_input_file(input_file):

    wires = []

    with open(input_file) as file:
        wire_strings = file.read().split()

    for wire_string in wire_strings:
        wire = []
        for step in wire_string.split(','):
            direction = step[:1]
            distance = int(step[1:])
            wire.append((direction, distance))

        wires.append(wire)

    return wires

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    args = parser.parse_args()

    wires = parse_input_file(args.input)

    wire_sets = []

    for wire in wires:
        wire_sets.append(draw_wire(wire))

    common = wire_sets[0] & wire_sets[1]

    min_dist = 100000000
    for coord in list(common)[1:]:
        dist = distance.cityblock((0,0), coord)
        if dist < min_dist:
            min_dist = dist

    print(min_dist)
