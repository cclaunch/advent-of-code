#!/usr/bin/env python
import argparse

def run_program(input_file):

    program = []

    with open(input_file) as file:
        program = [int(x) for x in file.read().split()[0].split(',')]

    program[1] = 12
    program[2] = 2

    index = 0
    opcode = program[index]

    while True:

        if opcode == 1:
            program[program[index+3]] = program[program[index+1]] + program[program[index+2]]

        if opcode == 2:
            program[program[index+3]] = program[program[index+1]] * program[program[index+2]]

        index = index + 4
        opcode = program[index]

        if opcode == 99:
            break

    return program[0]

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="puzzle input")
    args = parser.parse_args()

    print(run_program(args.input))
