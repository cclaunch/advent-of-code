#!/usr/bin/env python
import argparse
import copy

def run_analysis(input_file):

    program = []

    with open(input_file) as file:
        program = [int(x) for x in file.read().split()[0].split(',')]

    for i in range(100):
        for j in range(100):
            prog_copy = copy.deepcopy(program)
            try:
                output = run_program(prog_copy, i, j)
                if output == 19690720:
                    return [i, j]
            except:
                pass

def run_program(program, noun, verb):

    program[1] = noun
    program[2] = verb

    index = 0
    opcode = program[index]

    while True:

        if opcode == 1:
            program[program[index+3]] = program[program[index+1]] + program[program[index+2]]

        if opcode == 2:
            program[program[index+3]] = program[program[index+1]] * program[program[index+2]]

        index = index + 4
        opcode = program[index]

        if opcode == 99:
            break

    return program[0]

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="puzzle input")
    args = parser.parse_args()

    print(run_analysis(args.input))
