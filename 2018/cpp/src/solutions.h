#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <queue>

int32_t day1Part2(const std::string& inputFile)
{
    int32_t freq = 0;
    int32_t total = 0;
    std::vector<int32_t> frequencyChanges;
    std::vector<int32_t> frequencies;

    // read the input file into a vector
    std::ifstream file(inputFile);

    while (file >> freq)
    {
        frequencyChanges.push_back(freq);
    }

    while (true)
    {
        // generate all the frequencies by keeping a running total
        for (std::vector<int32_t>::const_iterator f = frequencyChanges.begin(); f != frequencyChanges.end(); ++f)
        {
            total += *f;

            // look for the first repeat frequency
            if (std::find(frequencies.begin(), frequencies.end(), total) != frequencies.end())
            {
                goto Found;
            }

            frequencies.push_back(total);
        }
    }

Found:
    return total;
}

uint32_t day1Part2Fast(const std::string& inputFile)
{
    int32_t freq = 0;
    int32_t total = 0;
    std::vector<int32_t> freqChanges;
    std::set<int32_t> freqs;

    // read the input file into a vector
    std::ifstream file(inputFile);

    while (file >> freq)
    {
        freqChanges.push_back(freq);
    }

    while (true)
    {
        // generate all the frequencies by keeping a running total
        for (std::vector<int32_t>::const_iterator f = freqChanges.begin(); f != freqChanges.end(); ++f)
        {
            total += *f;

            // look for the first repeat frequency
            if (freqs.find(total) != freqs.end())
            {
                goto Found;
            }

            // since we used a set instead of a vector, the entire thing stays sorted (way faster!)
            freqs.insert(total);
        }
    }

Found:
    return total;
}

std::string day2Part2(const std::string& inputFile)
{
    std::vector<std::string> boxIds;
    std::string id;
    std::string answer;

    // read the input into an array
    std::ifstream file(inputFile);

    while (file >> id)
    {
        boxIds.push_back(id);
    }

    // loop over all the ids twice
    for (std::vector<std::string>::const_iterator a = boxIds.begin(); a != boxIds.end(); ++a)
    {
        for (std::vector<std::string>::const_iterator b = boxIds.begin(); b != boxIds.end(); ++b)
        {
            uint8_t diff = 0;
            std::string::size_type diffIdx = 0;

            // exclude the id we're currently checking (duplicate)
            if (*a == *b)
            {
                continue;
            }

            // loop through every character in the id and check for differences
            for (std::string::size_type i = 0; i < (*a).size(); ++i)
            {
                if ((*a)[i] != (*b)[i])
                {
                    diff += 1;
                    diffIdx = i;

                    // if we find more than one different character, this isn't the right answer
                    if (diff > 2)
                    {
                        continue;
                    }
                }
            }

            // if we ended up with only one different character, we found it
            if (diff == 1)
            {
                answer = *a;

                // as per the instructions, remove the common character
                answer.erase(diffIdx, 1);

                goto Found;
            }
        }
    }

Found:
    return answer;
}

// helper for day 6
// yes, I could have input a std::pair, whatever, laziness
static inline unsigned manhattan_dist(int x1, int y1, int x2, int y2)
{
    return std::abs(x2 - x1) + std::abs(y2 - y1);
}

int day6Part1(const std::string& inputFile)
{
    std::vector<std::pair<int, int>> points;
    std::vector<std::vector<int>> grid;
    std::vector<int> validIdxs;
    std::string id;
    int xMax = 0;
    int yMax = 0;
    int xMin = 10000;
    int yMin = 10000;

    // read the input into an array
    std::ifstream file(inputFile);
    std::string line_data;

    while (getline(file, line_data))
    {
        int a, b; char comma;
        std::stringstream line_stream(line_data);

        if (line_stream >> a >> comma >> b)
        {
            points.push_back(std::pair<int, int>(b, a));
        }
    }

    // find the mins and maxes to iterate over
    for (auto i = points.begin(); i != points.end(); ++i)
    {
        if ((*i).first > xMax)
        {
            xMax = (*i).first;
        }

        if ((*i).second > yMax)
        {
            yMax = (*i).second;
        }

        if ((*i).first < xMin)
        {
            xMin = (*i).first;
        }

        if ((*i).second < yMin)
        {
            yMin = (*i).second;
        }
    }

    // zero out the grid array with -1 so we can track which points are
    // "owned" by which coordinates
    for (int i = 0; i < xMax; ++i)
    {
        std::vector<int> tmp(yMax, -1);
        grid.push_back(tmp);
    }

    // iterate over all the points in the grid
    for (int x = xMin; x < xMax; ++x)
    {
        for (int y = yMin; y < yMax; ++y)
        {
            int minDist = 1000;

            // check each coordinates distances to this point
            for (unsigned int i = 0; i < points.size(); ++i)
            {
                int dist = manhattan_dist(x, y, points[i].first, points[i].second);

                // find the lowest distance, if it's equal to another
                // coord's distance, then neither owns it (-1)
                if (dist < minDist)
                {
                    grid[x][y] = i;
                    minDist = dist;
                }
                else if (dist == minDist)
                {
                    grid[x][y] = -1;
                }
            }
        }
    }

    // now find which coords are valid (bounded on all four sides
    // by another coord)
    for (unsigned int i = 0; i < points.size(); ++i)
    {
        int bounds[4] = {0, 0, 0, 0};

        for (unsigned int j = 0; j < points.size(); ++j)
        {
            if (points[j].first < points[i].first)
            {
                bounds[0] = 1;
            }

            if (points[j].first > points[i].first)
            {
                bounds[1] = 1;
            }

            if (points[j].second < points[i].second)
            {
                bounds[2] = 1;
            }

            if (points[j].second > points[i].second)
            {
                bounds[3] = 1;
            }

            if (bounds[0] + bounds[1] + bounds[2] + bounds[3] == 4)
            {
                // only add a given point once
                std::vector<int>::iterator it;
                it = find (validIdxs.begin(), validIdxs.end(), i);

                if (it == validIdxs.end())
                {
                    validIdxs.push_back(i);
                }
            }
        }
    }

    // find the maximum area any given point owns and return
    int maxArea = 0;

    for (auto i = validIdxs.begin(); i != validIdxs.end(); ++i)
    {
        int tmpArea = 0;

        for (int x = 0; x < xMax; ++x)
        {
            for (int y = 0; y < yMax; ++y)
            {
                if (grid[x][y] == *i)
                {
                    tmpArea++;
                }
            }
        }

        if (tmpArea > maxArea)
        {
            maxArea = tmpArea;
        }
    }

    return maxArea;
}

int day6Part2(const std::string& inputFile)
{
    std::vector<std::pair<int, int>> points;
    std::vector<std::vector<int>> grid;
    std::vector<int> validIdxs;
    std::string id;
    int xMax = 0;
    int yMax = 0;
    int xMin = 10000;
    int yMin = 10000;

    // read the input into an array
    std::ifstream file(inputFile);
    std::string line_data;

    while (getline(file, line_data))
    {
        int a, b; char comma;
        std::stringstream line_stream(line_data);

        if (line_stream >> a >> comma >> b)
        {
            points.push_back(std::pair<int, int>(b, a));
        }
    }

    // find the mins and maxes to iterate over
    for (auto i = points.begin(); i != points.end(); ++i)
    {
        if ((*i).first > xMax)
        {
            xMax = (*i).first;
        }

        if ((*i).second > yMax)
        {
            yMax = (*i).second;
        }

        if ((*i).first < xMin)
        {
            xMin = (*i).first;
        }

        if ((*i).second < yMin)
        {
            yMin = (*i).second;
        }
    }

    // initialize the grid (to zero this time since we're adding up
    // total distances)
    for (int i = 0; i < xMax; ++i)
    {
        std::vector<int> tmp(yMax, 0);
        grid.push_back(tmp);
    }

    // iterate over all the relevant coords and total up the distances
    // to each point
    for (int x = xMin; x < xMax; ++x)
    {
        for (int y = yMin; y < yMax; ++y)
        {
            int minDist = 1000;

            for (unsigned int i = 0; i < points.size(); ++i)
            {
                int dist = manhattan_dist(x, y, points[i].first, points[i].second);

                grid[x][y] += dist;
            }
        }
    }

    // find all the distances less than 10k and add those
    // points as a valid space in the total area
    int totalArea = 0;

    for (int x = xMin; x < xMax; ++x)
    {
        for (int y = yMin; y < yMax; ++y)
        {
            if (grid[x][y] < 10000)
            {
                totalArea++;
            }
        }
    }

    return totalArea;
}

int day9Part2Failed(int playerCount, int finalMarble)
{
    std::vector<int> marbles;
    marbles.push_back(0);
    int currentMarble = 0;
    int currentMarbleIdx = 0;
    int playerIdx = 0;
    std::vector<int> scores;
    scores.resize(playerCount);
    int nextMarble = 0;
    int nextMarbleIdx = 0;

    while (true)
    {
        nextMarble = currentMarble + 1;

        if (nextMarble > finalMarble)
        {
            break;
        }

        if ((nextMarble % 23) == 0)
        {
            scores[playerIdx] += nextMarble;
            nextMarbleIdx = ((currentMarbleIdx - 7 + marbles.size() - 1) % marbles.size()) + 1;
            scores[playerIdx] += marbles[nextMarbleIdx];
            marbles.erase(marbles.begin() + nextMarbleIdx);
        }
        else
        {
            nextMarbleIdx = (currentMarbleIdx + 1) % marbles.size() + 1;
            marbles.insert(marbles.begin() + nextMarbleIdx, nextMarble);
        }

        currentMarble = nextMarble;
        currentMarbleIdx = nextMarbleIdx;


        // for (auto i = marbles.begin(); i != marbles.end(); ++i)
        // {
        //     std::cout << *i << " ";
        // }

        // std::cout << std::endl;

        playerIdx++;

        if (playerIdx >= playerCount)
        {
            playerIdx = 0;
        }
    }

    // for (auto i = scores.begin(); i != scores.end(); ++i)
    // {
    //     std::cout << *i << std::endl;
    // }

    return *std::max_element(std::begin(scores), std::end(scores));
}

char directions[4] = {'^', '>', 'v', '<'};
enum TurnRules {LEFT, STRAIGHT, RIGHT};

class Cart
{
public:

    Cart(const char& direction_in, int x, int y): xPos(x), yPos(y), rules(TurnRules::LEFT)
    {
        matchDirection(direction_in);
    }

    void turnRight()
    {
        if (direction == 3)
        {
            direction = 0;
        }
        else
        {
            direction++;
        }
    }

    void turnLeft()
    {
        if (direction == 0)
        {
            direction = 3;
        }
        else
        {
            direction--;
        }
    }

    void matchDirection(const char& c)
    {
        switch (c)
        {
            case '^':
                direction = 0;
                break;

            case '>':
                direction = 1;
                break;

            case 'v':
                direction = 2;
                break;

            case '<':
                direction = 3;
                break;
        }
    }

    void intersection()
    {
        switch (rules)
        {
            case TurnRules::LEFT:
                turnLeft();
                rules = TurnRules::STRAIGHT;
                break;

            case TurnRules::STRAIGHT:
                rules = TurnRules::RIGHT;
                break;

            case TurnRules::RIGHT:
                turnRight();
                rules = TurnRules::LEFT;
                break;
        }
    }

    void move()
    {
        switch (direction)
        {
            case 0:
                xPos--;
                break;

            case 1:
                yPos++;
                break;

            case 2:
                xPos++;
                break;

            case 3:
                yPos--;
                break;
        }
    }

    void print()
    {
        std::cout << "Cart: " << directions[direction] << " (" << xPos << ", " << yPos << ")" << std::endl;
    }

    int direction;
    TurnRules rules;
    int xPos;
    int yPos;
};

void printTrack(const std::vector<std::vector<char>>& track)
{
    int index = 0;

    for (auto i = track.begin(); i != track.end(); ++i)
    {
        std::cout << std::setw(3) << index << " " << std::string((*i).begin(), (*i).end()) << std::endl;
        index++;
    }
}

bool isCart(const char& c)
{
    std::string cartChars("^>v<");
    std::size_t found = cartChars.find_first_of(c);

    return found != std::string::npos;
}

void fillTrack(int x, int y, std::vector<std::vector<char>>& track)
{
    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;

    char cLeft = ' ';
    char cRight = ' ';
    char cUp = ' ';
    char cDown = ' ';

    std::string checkStr("<>^v ");

    if ((x - 1) >= 0)
    {
        if (checkStr.find(track[y][x - 1]) == std::string::npos)
        {
            left = true;
            cLeft = track[y][x - 1];
        }
    }

    if ((x + 1) < track[y].size())
    {
        if (checkStr.find(track[y][x + 1]) == std::string::npos)
        {
            right = true;
            cRight = track[y][x + 1];
        }
    }

    if ((y + 1) < track.size())
    {
        if (checkStr.find(track[y + 1][x]) == std::string::npos)
        {
            down = true;
            cDown = track[y + 1][x];
        }
    }

    if (y - 1 >= 0)
    {
        if (checkStr.find(track[y - 1][x]) == std::string::npos)
        {
            up = true;
            cUp = track[y - 1][x];
        }
    }

    if (up && down && left && right)
    {
        if (cUp == '|' || cUp == '+')
        {
            if (cLeft == '-' || cLeft == '+')
            {
                track[y][x] = '+';
            }
            else
            {
                track[y][x] = '|';
            }
        }
        else if (cLeft == '-' || cLeft == '+')
        {
            track[y][x] = '-';
        }
    }
    else if (up && down && !left && !right)
    {
        track[y][x] = '|';
    }
    else if (!up && !down && left && right)
    {
        track[y][x] = '-';
    }
    else if ((!up && down && !left && right) || (up && !down && left && !right))
    {
        track[y][x] = '/';
    }
    else if ((!up && down && left && !right) || (up && !down && !left && right))
    {
        track[y][x] = '\\';
    }
}

int day13Part2(const std::string& inputFile)
{

    std::vector<std::vector<char>> track;
    std::vector<std::string> lines;
    std::vector<Cart> carts;

    // read the input into an array
    std::ifstream file(inputFile);
    std::string line_data;
    unsigned int maxX = 0;
    unsigned int maxY = 0;

    while (getline(file, line_data))
    {
        lines.push_back(line_data);

        if (line_data.length() > maxX)
        {
            maxX = line_data.length();
        }
    }

    maxY = lines.size();


    std::cout << "maxX: " << maxX << std::endl;
    std::cout << "maxY: " << maxY << std::endl;

    track.resize(maxY);

    for (unsigned int i = 0; i < track.size(); ++i)
    {
        track[i].resize(maxX);
    }

    for (unsigned int i = 0; i < lines.size(); ++i)
    {
        for (unsigned int j = 0; j < lines[i].size(); ++j)
        {
            track[i][j] = lines[i].at(j);

            if (isCart(track[i][j]))
            {
                carts.push_back(Cart(track[i][j], i, j));
            }
        }
    }

    std::cout << std::endl << "initial Track: " << std::endl;
    printTrack(track);

    std::cout << std::endl << "initial Carts: " << std::endl;

    for (auto i = carts.begin(); i != carts.end(); ++i)
    {
        (*i).print();
        // fillTrack((*i).yPos, (*i).xPos, track);
    }

    // std::cout << std::endl;

    // printTrack(track);

    unsigned int tick = 0;

    while (true)
    {
        // std::cout << "tick " << tick << " " << carts.size() << std::endl;

        std::vector<unsigned int> crashed;

        for (unsigned int i = 0; i < carts.size(); ++i)
        {
            carts[i].move();
            char c = track[carts[i].xPos][carts[i].yPos];

            std::string cartChars("^>v<");
            std::size_t found = cartChars.find_first_of(c);

            if (found != std::string::npos)
            {
                carts[i].matchDirection(c);
            }
            else if (c == '+')
            {
                carts[i].intersection();
            }
            else if (c == '\\')
            {
                if (carts[i].direction == 1 || carts[i].direction == 3)
                {
                    carts[i].turnRight();
                }
                else
                {
                    carts[i].turnLeft();
                }
            }
            else if (c == '/')
            {
                if (carts[i].direction == 1 || carts[i].direction == 3)
                {
                    carts[i].turnLeft();
                }
                else
                {
                    carts[i].turnRight();
                }
            }

            for (unsigned int j = 0; j < carts.size(); ++j)
            {
                if (carts[j].xPos == carts[i].xPos && carts[j].yPos == carts[i].yPos && i != j)
                {
                    if (std::find(crashed.begin(), crashed.end(), i) == crashed.end())
                    {
                        crashed.push_back(i);
                        crashed.push_back(j);
                    }
                }
            }
        }

        if (carts.size() == 1)
        {
            std::cout << "FINAL CART!" << std::endl;
            carts[0].print();
            break;
        }


        // for (unsigned int i = 0; i < carts.size(); ++i)
        // {
        //     int crashCount = 0;

        //     for (auto j = carts.begin(); j != carts.end(); ++j)
        //     {
        //         if ((*j).xPos == carts[i].xPos && (*j).yPos == carts[i].yPos)
        //         {
        //             if (crashCount >= 1)
        //             {
        //                 if (std::find(crashed.begin(), crashed.end(), i) == crashed.end())
        //                 {
        //                     crashed.push_back(i);
        //                 }
        //             }
        //             else
        //             {
        //                 crashCount++;
        //             }
        //         }
        //     }
        // }

        std::sort(crashed.begin(), crashed.end());
        std::reverse(crashed.begin(), crashed.end());

        if (crashed.size() > 0)
        {
            std::cout << "tick: " << tick << " crashed: " << std::endl;

            for (auto i = carts.begin(); i != carts.end(); ++i)
            {
                (*i).print();
            }

            for (auto i = crashed.begin(); i < crashed.end(); ++i)
            {
                std::cout << *i << ", ";
                carts.erase(carts.begin() + *i);
            }

            std::cout << std::endl;
            std::cout << carts.size() << " carts remaining" << std::endl << std::endl;
        }

        tick++;
    }

    // for (auto i = carts.begin(); i != carts.end(); ++i)
    // {
    //     (*i).print();
    // }

    return 1;
};

enum PlayerType {ELF, GOBLIN};

class Player
{
public:
    Player(PlayerType type_in, int row_in, int column_in, int order_in): alive(true),
        type(type_in), row(row_in), column(column_in), order(order_in), hp(200)
    {
        if (type == PlayerType::ELF)
        {
            typeChar = 'E';
        }
        else
        {
            typeChar = 'G';
        }
    }

    void print()
    {
        std::cout << typeChar << " hp(" << hp << ") (" << row << ", " << column << ") order: " << order << std::endl;
    }

    bool operator<(const Player& p) const
    {
        return (order < p.order);
    }

    int hp;
    int row;
    int column;
    int order;
    PlayerType type;
    char typeChar;
    bool alive;
};

char getLeft(int row, int column, std::vector<std::vector<char>> field)
{
    if (column - 1 >= 0)
    {
        return field[row][column - 1];
    }

    return '?';
}

char getRight(int row, int column, std::vector<std::vector<char>> field)
{
    if (column + 1 < field[0].size())
    {
        return field[row][column + 1];
    }

    return '?';
}

char getUp(int row, int column, std::vector<std::vector<char>> field)
{
    if (row - 1 >= 0)
    {
        return field[row - 1][column];
    }

    return '?';
}

char getDown(int row, int column, std::vector<std::vector<char>> field)
{
    if (row + 1 < field.size())
    {
        return field[row + 1][column];
    }

    return '?';
}


// binary path maze distance calculation stolen from
// here: https://www.geeksforgeeks.org/shortest-path-in-a-binary-maze/

// A Data Structure for queue used in BFS
struct queueNode
{
    int row;
    int column;
    int dist;  // cell's distance of from the source
};


// These arrays are used to get row and column
// numbers of 4 neighbours of a given cell
int rowNum[] = { -1, 0, 0, 1};
int colNum[] = {0, -1, 1, 0};

int shortestPath(int pRow, int pColumn, int row, int column, std::vector<std::vector<char>> field)
{
    std::vector<std::vector<bool>> visited;
    visited.resize(field.size());

    for (int i = 0; i < visited.size(); ++i)
    {
        visited[i].resize(field[0].size());

        for (int j = 0; j < visited.size(); ++j)
        {
            visited[i][j] = false;
        }
    }

    visited[pRow][pColumn] = true;

    std::queue<queueNode> q;

    queueNode s = {pRow, pColumn, 0};
    q.push(s);

    while (!q.empty())
    {
        queueNode curr = q.front();
        int r = curr.row;
        int c = curr.column;

        if (r == row && c == column)
        {
            return curr.dist;
        }

        q.pop();

        for (int i = 0; i < 4; i++)
        {
            int rr = r + rowNum[i];
            int cc = c + colNum[i];

            if (rr < field.size() && rr >= 0 && cc < field[0].size() && cc >= 0 && !visited[rr][cc] && field[rr][cc] == '.')
            {
                visited[rr][cc] = true;

                queueNode Adjcell = {rr, cc, curr.dist + 1};
                q.push(Adjcell);
            }
        }
    }

    return -1;
}

Player getPlayer(int row, int column, std::vector<Player> players)
{
    for (auto i = players.begin(); i != players.end(); ++i)
    {
        if ((*i).row == row && (*i).column == column)
        {
            return *i;
        }
    }
}

int day15(const std::string& inputFile, int elfAp)
{
    std::vector<std::vector<char>> field;
    std::vector<std::string> lines;
    std::vector<Player> players;

    // read the input into an array
    std::ifstream file(inputFile);
    std::string line_data;
    unsigned int maxX = 0;
    unsigned int maxY = 0;

    while (getline(file, line_data))
    {
        lines.push_back(line_data);

        if (line_data.length() > maxX)
        {
            maxX = line_data.length();
        }
    }

    maxY = lines.size();


    // std::cout << "maxX: " << maxX << std::endl;
    // std::cout << "maxY: " << maxY << std::endl;
    // std::cout << std::endl;

    field.resize(maxY);

    for (unsigned int i = 0; i < field.size(); ++i)
    {
        field[i].resize(maxX);
    }


    int order = 0;

    for (unsigned int i = 0; i < lines.size(); ++i)
    {
        for (unsigned int j = 0; j < lines[i].size(); ++j)
        {
            field[i][j] = lines[i].at(j);

            if (field[i][j] == 'G')
            {
                players.push_back(Player(PlayerType::GOBLIN, i, j, order));
                order++;
            }
            else if (field[i][j] == 'E')
            {
                players.push_back(Player(PlayerType::ELF, i, j, order));
                order++;
            }
        }
    }

    printTrack(field);

    // std::cout << "players" << std::endl;

    for (auto i = players.begin(); i != players.end(); ++i)
    {
        //(*i).print();
    }

    int round = 0;

    while (true)
    {


        // now sort the list so they go in the correct turn
        std::sort(players.begin(), players.end());

        // identify targets
        for (unsigned int playerIdx = 0; playerIdx < players.size(); ++playerIdx)
        {
            if (players[playerIdx].alive)
            {

                std::vector<int> targets;

                // std::cout << "********************" << std::endl;
                // std::cout << "player turn:" << std::endl;
                //p.print();

                for (unsigned int j = 0; j < players.size(); ++j)
                {
                    if (players[j].type != players[playerIdx].type && players[j].alive)
                    {
                        targets.push_back(j);
                    }
                }

                if (targets.size() == 0)
                {
                    goto endBattle;
                }

                std::vector<int> adjacents;

                // check for adjacents first
                for (int i = 0; i < 4; i++)
                {
                    int rr = players[playerIdx].row + rowNum[i];
                    int cc = players[playerIdx].column + colNum[i];
                    char c = ' ';

                    if (players[playerIdx].type == PlayerType::GOBLIN)
                    {
                        c = 'E';
                    }
                    else
                    {
                        c = 'G';
                    }

                    if (rr < field.size() && rr >= 0 && cc < field[0].size() && cc >= 0 && field[rr][cc] == c)
                    {
                        for (int j = 0; j < players.size(); ++j)
                        {
                            if (players[j].row == rr && players[j].column == cc && players[j].alive)
                            {
                                adjacents.push_back(j);
                            }
                        }
                    }
                }

                // attack!
                if (adjacents.size() > 0)
                {
                    std::vector<Player> enemies;

                    int minHp = 201;
                    int nextEnemy = -1;

                    for (auto j = adjacents.begin(); j != adjacents.end(); ++j)
                    {
                        if (players[*j].hp < minHp && players[*j].alive)
                        {
                            minHp = players[*j].hp;
                            nextEnemy = *j;
                        }

                        if (players[*j].hp == minHp && players[*j].order < players[nextEnemy].order && players[*j].alive)
                        {
                            nextEnemy = *j;
                        }
                    }

                    std::cout << "Attacker: ";
                    players[playerIdx].print();
                    std::cout << " Attacking: ";
                    players[nextEnemy].print();

                    // if (p.type == PlayerType::ELF)
                    // {
                    //     nextEnemy.hp -= elfAp;
                    // }
                    // else
                    // {
                    //     nextEnemy.hp -= 3;
                    // }
                    if (players[playerIdx].type == PlayerType::ELF)
                    {
                        players[nextEnemy].hp -= elfAp;
                    }
                    else
                    {
                        players[nextEnemy].hp -= 3;
                    }

                    if (players[nextEnemy].hp <= 0)
                    {
                        // std::cout << "Player killed!" << std::endl;
                        players[nextEnemy].alive = false;

                        // if (p.type == PlayerType::ELF)
                        // {
                        //     players[nextEnemy.order].hp -= elfAp;
                        // }
                        if (players[playerIdx].type == PlayerType::GOBLIN)
                        {
                            std::cout << "ELF DIED!!!" << std::endl;
                            // goto endBattle;
                            // players[nextEnemy.order].hp -= 3;
                        }

                        field[players[nextEnemy].row][players[nextEnemy].column] = '.';
                    }

                    // else
                    // {
                    //     if (p.type == PlayerType::ELF)
                    //     {
                    //         players[nextEnemy.order].hp -= elfAp;
                    //     }
                    //     else
                    //     {
                    //         players[nextEnemy.order].hp -= 3;
                    //     }
                    // }

                    // our turn is over, don't move
                    continue;
                }


                std::vector<std::pair<int, int>> inRange;

                for (auto i = targets.begin(); i != targets.end(); ++i)
                {
                    // Player t = *i;
                    //t.print();

                    if (getLeft(players[*i].row, players[*i].column, field) == '.')
                    {
                        inRange.push_back(std::make_pair(players[*i].row, players[*i].column - 1));
                    }

                    if (getRight(players[*i].row, players[*i].column, field) == '.')
                    {
                        inRange.push_back(std::make_pair(players[*i].row, players[*i].column + 1));
                    }

                    if (getUp(players[*i].row, players[*i].column, field) == '.')
                    {
                        inRange.push_back(std::make_pair(players[*i].row - 1, players[*i].column));
                    }

                    if (getDown(players[*i].row, players[*i].column, field) == '.')
                    {
                        inRange.push_back(std::make_pair(players[*i].row + 1, players[*i].column));
                    }
                }

                // std::cout << "inRange: " << std::endl;
                int minDist = 100000000;
                std::pair<int, int> nearest;

                for (auto i = inRange.begin(); i != inRange.end(); ++i)
                {
                    // std::cout << (*i).first << ", " << (*i).second << std::endl;
                    int dist = shortestPath(players[playerIdx].row, players[playerIdx].column, (*i).first, (*i).second, field);

                    if (dist >= 0)
                    {
                        if (dist < minDist)
                        {
                            minDist = dist;
                            nearest = *i;
                        }

                        if (dist == minDist)
                        {
                            if ((*i).first < nearest.first)
                            {
                                minDist = dist;
                                nearest = *i;
                            }
                            else if ((*i).first == nearest.first)
                            {
                                if ((*i).second < nearest.second)
                                {
                                    minDist = dist;
                                    nearest = *i;
                                }
                            }
                        }
                    }

                    // std::cout << "dist: " << dist << std::endl;
                }

                // std::cout << "nearest: " << nearest.first << ", " << nearest.second << std::endl;

                int minNextDist = 100000000;
                std::pair<int, int> nextStep;

                for (int i = 0; i < 4; i++)
                {
                    int rr = players[playerIdx].row + rowNum[i];
                    int cc = players[playerIdx].column + colNum[i];

                    if (rr < field.size() && rr >= 0 && cc < field[0].size() && cc >= 0 && field[rr][cc] == '.')
                    {
                        int dist = shortestPath(rr, cc, nearest.first, nearest.second, field);
                        // std::cout << "nextStep test: " << rr << ", " << cc << ": dist = " << dist << std::endl;

                        if (dist < 0)
                        {
                            continue;
                        }

                        if (dist < minNextDist)
                        {
                            minNextDist = dist;
                            nextStep = std::make_pair(rr, cc);
                        }

                        if (dist == minNextDist)
                        {
                            if (rr < nextStep.first)
                            {
                                minNextDist = dist;
                                nextStep = std::make_pair(rr, cc);;
                            }
                            else if (rr == nextStep.first)
                            {
                                if (cc < nextStep.second)
                                {
                                    minNextDist = dist;
                                    nextStep = std::make_pair(rr, cc);;
                                }
                            }
                        }
                    }
                }

                // std::cout << "nextStep: " << nextStep.first << ", " << nextStep.second << std::endl;

                if (minDist == 0)
                {
                    // attack!
                }
                else if (minDist != 100000000)
                {
                    // actually move the player
                    field[players[playerIdx].row][players[playerIdx].column] = '.';
                    players[playerIdx].row = nextStep.first;
                    players[playerIdx].column = nextStep.second;
                    field[nextStep.first][nextStep.second] = (players[playerIdx].type == PlayerType::GOBLIN) ? 'G' : 'E';
                }

                adjacents.clear();

                // check for adjacents first
                for (int i = 0; i < 4; i++)
                {
                    int rr = players[playerIdx].row + rowNum[i];
                    int cc = players[playerIdx].column + colNum[i];
                    char c = ' ';

                    if (players[playerIdx].type == PlayerType::GOBLIN)
                    {
                        c = 'E';
                    }
                    else
                    {
                        c = 'G';
                    }

                    if (rr < field.size() && rr >= 0 && cc < field[0].size() && cc >= 0 && field[rr][cc] == c)
                    {
                        for (int j = 0; j < players.size(); ++j)
                        {
                            if (players[j].row == rr && players[j].column == cc && players[j].alive)
                            {
                                adjacents.push_back(j);
                            }
                        }
                    }
                }

                // attack!
                if (adjacents.size() > 0)
                {
                    std::vector<Player> enemies;

                    int minHp = 201;
                    int nextEnemy = -1;

                    for (auto j = adjacents.begin(); j != adjacents.end(); ++j)
                    {
                        if (players[*j].hp < minHp && players[*j].alive)
                        {
                            minHp = players[*j].hp;
                            nextEnemy = *j;
                        }

                        if (players[*j].hp == minHp && players[*j].order < players[nextEnemy].order && players[*j].alive)
                        {
                            nextEnemy = *j;
                        }
                    }


                    std::cout << "Attacker: ";
                    players[playerIdx].print();
                    std::cout << " Attacking: ";
                    players[nextEnemy].print();
                    // if (p.type == PlayerType::ELF)
                    // {
                    //     nextEnemy.hp -= elfAp;
                    // }
                    // else
                    // {
                    //     nextEnemy.hp -= 3;
                    // }

                    if (players[playerIdx].type == PlayerType::ELF)
                    {
                        players[nextEnemy].hp -= elfAp;
                    }
                    else
                    {
                        players[nextEnemy].hp -= 3;
                    }

                    if (players[nextEnemy].hp <= 0)
                    {
                        // std::cout << "Player killed!" << std::endl;
                        players[nextEnemy].alive = false;

                        if (players[playerIdx].type == PlayerType::GOBLIN)
                        {
                            std::cout << "ELF DIED!!!" << std::endl;
                            // goto endBattle;
                            // players[nextEnemy.order].hp -= 3;
                        }

                        field[players[nextEnemy].row][players[nextEnemy].column] = '.';
                    }

                    // else
                    // {
                    //     if (p.type == PlayerType::ELF)
                    //     {
                    //         players[nextEnemy.order].hp -= elfAp;
                    //     }
                    //     else
                    //     {
                    //         players[nextEnemy.order].hp -= 3;
                    //     }
                    // }
                }
            }
        }



        // std::vector<int> deadPlayers;

        // for (int i = 0; i < players.size(); ++i)
        // {
        //     if (!players[i].alive)
        //     {
        //         deadPlayers.push_back(i);
        //     }
        // }

        // std::sort(deadPlayers.begin(), deadPlayers.end());
        // std::reverse(deadPlayers.begin(), deadPlayers.end());

        // for (auto i = deadPlayers.begin(); i != deadPlayers.end(); ++i)
        // {
        //     field[players[*i].row][players[*i].column] = '.';
        //     players.erase(players.begin() + *i);
        // }

        // reorder players based on new locations
        int newOrder = 0;

        for (unsigned int i = 0; i < field.size(); ++i)
        {
            for (unsigned int j = 0; j < field[i].size(); ++j)
            {
                char c = field[i][j];

                if (c == 'G' || c == 'E')
                {
                    for (unsigned int k = 0; k < players.size(); ++k)
                    {
                        if (!players[k].alive)
                        {
                            players[k].order = 10000;
                        }
                        else if (players[k].row == i && players[k].column == j)
                        {
                            players[k].order = newOrder;
                            newOrder++;
                        }
                    }
                }
            }
        }

        std::sort(players.begin(), players.end());
        // was here

        printTrack(field);

        std::cout << "players" << std::endl;

        for (auto i = players.begin(); i != players.end(); ++i)
        {
            (*i).print();
        }

        round++;
        std::cout << "******** END ROUND " << round << " *********" << std::endl;

    }

endBattle:
    std::cout << "players" << std::endl;
    int total = 0;

    for (auto i = players.begin(); i != players.end(); ++i)
    {
        (*i).print();
        total += (*i).hp;
    }

    std::cout << "********* END BATTLE! ***********" << std::endl;
    std::cout << "hptotal: " << total << std::endl;
    std::cout << "rounds: " << round << std::endl;

    total *= (round);

    printTrack(field);




    std::cout << "solution: " << total << std::endl;
    return 1;
}

// These arrays are used to get row and column
// numbers of 4 neighbours of a given cell
int rowChecker[] = { -1, 0, 0, 1, -1, -1, 1, 1};
int colChecker[] = {0, -1, 1, 0, -1, 1, -1, 1};

int day18(const std::string& inputFile)
{
    std::vector<std::vector<char>> field;
    std::vector<std::vector<char>> nextField;
    std::vector<std::string> lines;

    // read the input into an array
    std::ifstream file(inputFile);
    std::string line_data;
    unsigned int maxX = 0;
    unsigned int maxY = 0;

    while (getline(file, line_data))
    {
        lines.push_back(line_data);

        if (line_data.length() > maxX)
        {
            maxX = line_data.length();
        }
    }

    maxY = lines.size();
    field.resize(maxY);
    nextField.resize(maxY);

    for (unsigned int i = 0; i < field.size(); ++i)
    {
        field[i].resize(maxX);
        nextField[i].resize(maxX);
    }

    for (unsigned int i = 0; i < lines.size(); ++i)
    {
        for (unsigned int j = 0; j < lines[i].size(); ++j)
        {
            field[i][j] = lines[i].at(j);
        }
    }


    std::cout << "maxX = " << maxX << " maxY = " << maxY << std::endl;

    printTrack(field);

    int minute = 0;
    int prevTotal = 0;
    int num1 = -1745;
    int num2 = -3513;

    while (minute < 1000)
    {
        for (int c = 0; c < field.size(); ++c)
        {
            for (int r = 0; r < field[c].size(); ++r)
            {
                int treeCount = 0;
                int lumberCount = 0;

                for (int i = 0; i < 8; i++)
                {
                    int rr = r + rowChecker[i];
                    int cc = c + colChecker[i];

                    if (rr < field.size() && rr >= 0 && cc < field[0].size() && cc >= 0)
                    {
                        if (field[rr][cc] == '|')
                        {
                            treeCount++;
                        }
                        else if (field[rr][cc] == '#')
                        {
                            lumberCount++;
                        }
                    }
                }

                if (field[r][c] == '.')
                {
                    if (treeCount >= 3)
                    {
                        nextField[r][c] = '|';
                    }
                    else
                    {
                        nextField[r][c] = '.';
                    }
                }
                else if (field[r][c] == '|')
                {
                    if (lumberCount >= 3)
                    {
                        nextField[r][c] = '#';
                    }
                    else
                    {
                        nextField[r][c] = '|';
                    }
                }
                else
                {
                    if (lumberCount >= 1 && treeCount  >= 1)
                    {
                        nextField[r][c] = '#';
                    }
                    else
                    {
                        nextField[r][c] = '.';
                    }
                }
            }
        }

        for (int c = 0; c < field.size(); ++c)
        {
            for (int r = 0; r < field[c].size(); ++r)
            {
                field[r][c] = nextField[r][c];
            }
        }

        minute++;

        // if (minute % 1000 == 0)
        // {
        // std::cout << "end of minute " << minute << std::endl;
        // printTrack(field);
        int lumberCount = 0;
        int treeCount = 0;

        for (auto i = field.begin(); i != field.end(); ++i)
        {
            lumberCount += std::count((*i).begin(), (*i).end(), '#');
            treeCount += std::count((*i).begin(), (*i).end(), '|');
        }

        // std::cout << "treeCount = " << treeCount << std::endl;
        // std::cout << "lumberCount = " << lumberCount << std::endl;
        int total = lumberCount * treeCount;
        int diff = total - prevTotal;

        if (diff == num1)
        {
            num1 = 0;
        }

        if (diff == num2 && num1 == 0)
        {
            std::cout << "found start of pattern at: " << minute << std::endl;
            num1 = -1;
            // break;
        }

        std::cout << "total = " << total << std::endl;
        // std::cout << "diff = " << diff << std::endl;
        prevTotal = total;
        // }
    }

    return 0;
}