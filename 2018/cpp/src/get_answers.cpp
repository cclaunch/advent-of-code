#include <iostream>
#include "solutions.h"

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0] << " INPUTFOLDER" << std::endl;
        return 1;
    }

    // std::cout << "day1Part2: " << day1Part2Fast(std::string(argv[1]) + std::string("/day01")) << std::endl;
    // std::cout << "day2Part2: " << day2Part2(std::string(argv[1]) + std::string("/day02")) << std::endl;
    // std::cout << "day6Part1: " << day6Part1(std::string(argv[1]) + std::string("/day06")) << std::endl;
    // std::cout << "day6Part2: " << day6Part2(std::string(argv[1]) + std::string("/day06")) << std::endl;
    // day13Part2(std::string(argv[1]) + std::string("/day13"));
    day15(std::string(argv[1]) + std::string("/day15"), 10);
    // day18(std::string(argv[1]) + std::string("/day18"));
}
