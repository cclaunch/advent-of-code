#include <gtest/gtest.h>
#include "solutions.h"

TEST(Year2018, Day1Part2)
{
    EXPECT_EQ(76787, day1Part2Fast("day01"));
}

TEST(Year2018, Day2Part2)
{
    EXPECT_EQ("zihwtxagifpbsnwleydukjmqv", day2Part2("day02"));
}

TEST(Year2018, Day6Part1)
{
    EXPECT_EQ(4771, day6Part1("day06"));
}

TEST(Year2018, Day6Part2)
{
    EXPECT_EQ(39149, day6Part2("day06"));
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
