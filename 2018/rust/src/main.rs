#[macro_use] extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::io::{BufRead, BufReader, Error, ErrorKind, Read};

// read the input file into a vector
fn day1_part2_read_input<R: Read>(io: R) -> Result<Vec<i32>, Error> {
    let br = BufReader::new(io);
    br.lines()
        .map(|line| line.and_then(|v| v.parse().map_err(|e| Error::new(ErrorKind::InvalidData, e))))
        .collect()
}

pub fn day1_part2(input_file: &PathBuf) -> Result<i32, Error> {
    let mut total: i32 = 0;
    let mut freqs: Vec<i32> = Vec::new();
    let _freq_changes = day1_part2_read_input(File::open(input_file)?)?;
    
    // labeled loop, keep running total
    'outer: loop {
        for f in &_freq_changes {
            total += f;

            // look for the repeated frequency
            match freqs.binary_search(&total) {
                Ok(_pos) => {
                    // break out of labeled loop
                    break 'outer;
                }
                Err(pos) => freqs.insert(pos, total),
            }
        }
    }

    Ok(total)
}

pub fn day5_part2(input_file: &PathBuf) -> Result<usize, Error> {
    let mut file = match File::open(&input_file) {
        Ok(f) => f,
        Err(err) => panic!("file error: {}", err)
    };

    // read the whole file as one big string
    let mut polymer = String::new();
    file.read_to_string(&mut polymer)?;

    // generate the crazy regex.  note: rust doesn't support some of the 
    // fancier regexes.  static so the regex doesn't get recompiled each loop
    lazy_static! {
        static ref ridiculous_regex: Regex = Regex::new(
            r"aA|Aa|bB|Bb|cC|Cc|dD|Dd|eE|Ee|fF|Ff|gG|Gg|hH|Hh|iI|Ii|jJ|Jj|kK|Kk|lL|Ll|mM|Mm|nN|Nn|oO|Oo|pP|Pp|qQ|Qq|rR|Rr|sS|Ss|tT|Tt|uU|Uu|vV|Vv|wW|Ww|xX|Xx|yY|Yy|zZ|Zz").unwrap();
    }

    let mut min_poly_size = polymer.len();
    let mut poly: String;
    let mut prev_len = 0;

    // test by remove all of each character in the alphabet
    let alphabet = ('a' as u8..'z' as u8 + 1)
        .map(|i| i as char)
        .collect::<Vec<_>>();

    for c in alphabet {
        poly = polymer.clone();

        // the regex is just any upper/lowercase version of the
        // given letter
        let alpha_regex_text = format!("(?i){}", c.to_string());
        let alpha_regex = Regex::new(&alpha_regex_text).unwrap();
        poly = alpha_regex.replace_all(&poly, "").to_string();
        
        // now do the polymer matching
        while prev_len != poly.len() {
            prev_len = poly.len();
            poly = ridiculous_regex.replace_all(&poly, "").to_string();
        }

        // keep track of the smallest size
        if poly.len() < min_poly_size {
            min_poly_size = poly.len();
        }
    }

    Ok(min_poly_size)
}


fn main() -> Result<(), Error> {
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        println!("Please provide an input file directory");
        std::process::exit(1);
    }

    let input_dir_path = Path::new(&args[1]);

    println!("day1Part2: {}", day1_part2(&input_dir_path.join("day01"))?);
    println!("day5Part2: {}", day5_part2(&input_dir_path.join("day05"))?);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn day1_part2_check() {
        assert_eq!(76787, day1_part2(&PathBuf::from("../input/day01")).unwrap());
    }

    #[test]
    fn day5_part2_check() {
        assert_eq!(4240, day5_part2(&PathBuf::from("../input/day05")).unwrap());
    }
}
