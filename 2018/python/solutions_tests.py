import unittest
from solutions import *


class TestAnswers(unittest.TestCase):

    def test_day1_part1(self):
        self.assertEqual(531, day1_part1('../input/day01'))

    def test_day1_part2(self):
        self.assertEqual(76787, day1_part2_fast('../input/day01'))

    def test_day2_part1(self):
        self.assertEqual(8892, day2_part1('../input/day02'))

    def test_day2_part2(self):
        self.assertEqual('zihwtxagifpbsnwleydukjmqv',
                         day2_part2('../input/day02'))

    def test_day3_part1(self):
        self.assertEqual(113966, day3_part1('../input/day03'))

    def test_day3_part2(self):
        self.assertEqual('235', day3_part2('../input/day03'))

    def test_day4_both(self):
        self.assertEqual([118599, 33949], day4_all('../input/day04'))

    def test_day5_part1(self):
        self.assertEqual(11946, day5_part1_fast('../input/day05'))

    def test_day5_part2(self):
        self.assertEqual(4240, day5_part2_fast('../input/day05'))

    def test_day7_part1(self):
        self.assertEqual('OVXCKZBDEHINPFSTJLUYRWGAMQ',
                         day7_part1('../input/day07'))

    def test_day8_part1(self):
        self.assertEqual(49602, day8_part1('../input/day08'))

    def test_day8_part2(self):
        self.assertEqual(25656, day8_part2('../input/day08'))

    def test_day9_part1(self):
        self.assertEqual(412127, day9(418, 71339))

    def test_day9_part2(self):
        self.assertEqual(3482394794, day9(418, 7133900))

if __name__ == '__main__':
    unittest.main()
