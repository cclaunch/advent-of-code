#!/usr/bin/env python
import argparse
import bisect
import datetime
import math
import re
import threading
from collections import Counter
from scipy.spatial import distance
from ast import literal_eval
from blist import blist


class Found(Exception):
    pass


def day1_part1(input_file):

    total = 0

    # open the file, add up all the lines, easy peasy
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            total = total + int(line)

    return total


def day1_part2(input_file):
    total = 0
    frequency_changes = []
    frequencies = []

    # grab all the frequencies into an array
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            frequency_changes.append(int(line))

    # generate the running frequency total
    try:
        while True:
            for f in frequency_changes:
                total = total + f
                # look for the first repeated frequency
                if total in frequencies:
                    raise Found
                frequencies.append(total)

    except Found:
        return total


def day1_part2_fast(input_file):
    total = 0
    frequency_changes = []
    frequencies = []

    # grab all the frequencies into an array
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            frequency_changes.append(int(line))

    # generate the running frequency total
    try:
        while True:
            for f in frequency_changes:
                total = total + f

                # search using bisect (list is already sorted, way faster)
                pos = bisect.bisect_left(frequencies, total)
                if pos != len(frequencies) and frequencies[pos] == total:
                    raise Found

                # use bisect to keep the array sorted while inserting
                bisect.insort(frequencies, total)

    except Found:
        return total


def day2_part1(input_file):

    twos = 0
    threes = 0

    # only going through the input once so no need
    # to save the input to an array
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            two_found = False
            three_found = False

            # Counter will actually do all the work for us!
            count = Counter(line)
            for letter, num in count.iteritems():
                if num == 2 and not two_found:
                    twos = twos + 1
                    two_found = True
                if num == 3 and not three_found:
                    threes = threes + 1
                    three_found = True

    return int(twos * threes)


def day2_part2(input_file):
    answer = ''
    box_ids = []

    # save the box ids to an array
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            box_ids.append(line.strip())

    try:
        # loop over all the ids twice
        for box_id1 in box_ids:
            for box_id2 in box_ids:
                diff = 0
                diff_idx = 0

                # exclude the id we're currently checking (duplicate)
                if box_id1 == box_id2:
                    continue
                # loop through every character in the id and check for
                # differences
                for i in range(len(box_id1)):
                    if box_id1[i] != box_id2[i]:
                        diff = diff + 1
                        diff_idx = i

                        # if we find more than one different character, this isn't
                        # the right answer
                        if diff > 2:
                            continue
                # if we ended up with only one different character, we found it
                if diff == 1:
                    # as per the instructions, remove the common character
                    answer = box_id1[:diff_idx] + box_id1[diff_idx + 1:]
                    raise Found

    except Found:
        return answer


def day3_part1(input_file):
    # create a 1000x1000 array to represent all the sq inches
    inches = [[0 for x in range(1000)] for y in range(1000)]

    # read the file and parse out all the needed bits
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            elems = line.split()
            coords = elems[2][:-1].split(',')
            size = elems[3].split('x')
            coord_x = int(coords[0])
            coord_y = int(coords[1])
            width = int(size[0])
            height = int(size[1])

            # loop through the square inches and increment each one covered
            for x in range(coord_x, coord_x + width):
                for y in range(coord_y, coord_y + height):
                    inches[x][y] = inches[x][y] + 1

    total_inches = 0

    # now simply add up all the incremented square inches
    for x in range(1000):
        for y in range(1000):
            if inches[x][y] > 1:
                total_inches = total_inches + 1

    return total_inches


def day3_part2(input_file):
    # create a 1000x1000 array to represent all the sq inches
    inches = [[0 for x in range(1000)] for y in range(1000)]

    # read the file and parse out all the needed bits
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            elems = line.split()
            coords = elems[2][:-1].split(',')
            size = elems[3].split('x')
            coord_x = int(coords[0])
            coord_y = int(coords[1])
            width = int(size[0])
            height = int(size[1])

            # loop through the square inches and increment each one covered
            for x in range(coord_x, coord_x + width):
                for y in range(coord_y, coord_y + height):
                    inches[x][y] = inches[x][y] + 1

    # read the file again, this time to check the entries against the actual
    with open(input_file) as file:
        for cnt, line in enumerate(file):
            elems = line.split()
            coords = elems[2][:-1].split(',')
            size = elems[3].split('x')
            coord_x = int(coords[0])
            coord_y = int(coords[1])
            width = int(size[0])
            height = int(size[1])

            # look for entries that don't overlap
            all_ones = True
            for x in range(coord_x, coord_x + width):
                for y in range(coord_y, coord_y + height):
                    # if there's any entries higher than 1, this one fails
                    if inches[x][y] > 1:
                        all_ones = False

            # if we made it all the way through then this is the answer!
            if all_ones:
                return elems[0][1:]


def day4_all(input_file):
    current_guard = 0
    current_sleep_time = 0

    # keep track of guards' total minutes asleep
    guards = {}

    # keep track of guards' individual minutes asleep
    guards_minutes_asleep = {}

    with open(input_file) as file:

        # sort the input first
        all_lines = sorted(file.readlines())

        for line in all_lines:
            # any # line is a new guard on shift
            if '#' in line:
                # lazy parsing since we know the format of the data
                current_guard = line.split()[3][1:]

                if current_guard not in guards_minutes_asleep:
                    # if it's a guard we haven't seen, we need "allocate" the
                    # individual minutes array
                    guards_minutes_asleep[current_guard] = [0] * 60
                    guards[current_guard] = 0
            elif 'fall' in line:
                # guard fell asleep
                current_sleep_time = int(line[15:17])
            else:
                # guard woke up
                minute = int(line[15:17])

                # increment the running total
                guards[current_guard] = guards[
                    current_guard] + minute - current_sleep_time

                # increment the individual minutes array for the guard
                for i in range(current_sleep_time, minute):
                    guards_minutes_asleep[current_guard][
                        i] = guards_minutes_asleep[current_guard][i] + 1

                current_sleep_time = 0

    # now reverse sort the dict to get the highest minutes guard
    guards = sorted(guards.items(), key=lambda x: x[1], reverse=True)
    guard_most = guards[0][0]

    max_minutes = 0
    max_idx = 0

    # for part 1, grab the highest minutes guard x the guard's highest single
    # minute
    for i in range(len(guards_minutes_asleep[guard_most])):
        if guards_minutes_asleep[guard_most][i] > max_minutes:
            max_minutes = guards_minutes_asleep[guard_most][i]
            max_idx = i

    part1_answer = int(guard_most) * max_idx

    max_minutes = 0
    max_idx = 0

    # for part 2, grab the highest single minute of all the guards, then
    # multiply
    for guard, minutes in guards_minutes_asleep.iteritems():
        for i in range(len(minutes)):
            if minutes[i] > max_minutes:
                max_minutes = minutes[i]
                guard_most = guard
                max_idx = i

    part2_answer = int(guard_most) * max_idx

    return [part1_answer, part2_answer]


def day5_part1(input_file):

    whole_polymer = []

    # read the whole polymer as a char array
    with open(input_file) as file:
        whole_polymer = list(file.read())

    c_prev = ''
    idx = 0
    poly_size = len(whole_polymer)

    try:
        while True:
            # keep track of our current polymer size bc the size changes during
            # iteration
            prev_poly_size = poly_size
            for i in range(poly_size - 1):
                # check for the character sequence
                if whole_polymer[i] == whole_polymer[i + 1].lower():
                    # make sure you pop the same index twice (size changes)
                    whole_polymer.pop(i)
                    whole_polymer.pop(i)
                    # change the array size
                    poly_size = poly_size - 2
                    break
                elif whole_polymer[i + 1] == whole_polymer[i].lower():
                    whole_polymer.pop(i)
                    whole_polymer.pop(i)
                    poly_size = poly_size - 2
                    break
            # if we get here, we know we didn't find any matches, so break the
            # loop
            if prev_poly_size == poly_size:
                raise Found

    except Found:
        return poly_size


def day5_part1_fast(input_file):

    whole_polymer = []

    # this time, read the whole polymer as a string
    with open(input_file) as file:
        whole_polymer = file.read()

    # build the regex with all the possible alpha combinations OR'd together
    ridiculous_regex = ''
    for i in range(26):
        ridiculous_regex = ridiculous_regex + \
            chr(ord('a') + i) + chr(ord('A') + i) + '|' + \
            chr(ord('A') + i) + chr(ord('a') + i) + '|'

    # remove the last OR pipe
    ridiculous_regex = ridiculous_regex[:-1]

    # we still have to iterate, but stop iterating once the size of the
    # polymer stops changing
    prev_len = len(whole_polymer)
    while True:
        whole_polymer = re.sub(ridiculous_regex, '', whole_polymer)
        if len(whole_polymer) == prev_len:
            break
        prev_len = len(whole_polymer)

    return len(whole_polymer)


def day5_part2(input_file):

    whole_polymer = []

    # read the whole polymer as a string
    with open(input_file) as file:
        whole_polymer = file.read()

    # build the regex with all the possible alpha combinations OR'd together
    ridiculous_regex = ''
    for i in range(26):
        ridiculous_regex = ridiculous_regex + \
            chr(ord('a') + i) + chr(ord('A') + i) + '|' + \
            chr(ord('A') + i) + chr(ord('a') + i) + '|'

    # remove the last OR pipe
    ridiculous_regex = ridiculous_regex[:-1]

    # keep track of the smallest size we find
    min_poly_size = len(whole_polymer)

    # test removing all characters of each in the alphabet
    for i in range(26):
        poly = whole_polymer

        # regex is just the letter
        alpha_regex = chr(ord('a') + i)

        # remove all instances of the letter (ignoring case)
        poly = re.sub(
            alpha_regex, '', poly, flags=re.IGNORECASE)
        prev_len = len(poly)

        # now do the polymer matching
        while True:
            poly = re.sub(ridiculous_regex, '', poly)
            if len(poly) == prev_len:
                break
            prev_len = len(poly)

        # keep track of the smallest size
        if len(poly) < min_poly_size:
            min_poly_size = len(poly)

    return min_poly_size


def day5_part2_fast_thread(regex, alpha, poly, results):

    # remove all instances of the letter (ignoring case)
    poly = re.sub(
        alpha, '', poly, flags=re.IGNORECASE)
    prev_len = len(poly)

    # now do the polymer matching
    while True:
        poly = re.sub(regex, '', poly)
        if len(poly) == prev_len:
            break
        prev_len = len(poly)

    results[alpha] = len(poly)


def day5_part2_fast(input_file):

    whole_polymer = []

    # read the whole polymer as a string
    with open(input_file) as file:
        whole_polymer = file.read()

    # build the regex with all the possible alpha combinations OR'd together
    ridiculous_regex = ''
    for i in range(26):
        ridiculous_regex = ridiculous_regex + \
            chr(ord('a') + i) + chr(ord('A') + i) + '|' + \
            chr(ord('A') + i) + chr(ord('a') + i) + '|'

    # remove the last OR pipe
    ridiculous_regex = ridiculous_regex[:-1]

    # keep track of the smallest size we find
    min_poly_size = len(whole_polymer)

    threads = []
    results = {}

    # test removing all characters of each in the alphabet, this time with
    # threads and passing in a common results dict
    for i in range(26):
        thread = threading.Thread(target=day5_part2_fast_thread, args=(
            ridiculous_regex, chr(ord('a') + i), whole_polymer, results))
        threads.append(thread)

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    return sorted(results.items(), key=lambda x: x[1])[0][1]


def day6_part1_fail(input_file):
    # this solution failed and I went to c++, don't care, not going to update
    # :)
    with open(input_file) as file:
        all_lines = [line.rstrip() for line in file]

        coords = []
        # max_dist = 0

        for line in all_lines:
            coords.append(list(literal_eval(line)))

        max_x = 0
        max_y = 0
        dists = [0] * len(coords)

        for coord in coords:
            if coord[0] > max_x:
                max_x = coord[0]
            if coord[1] > max_y:
                max_y = coord[1]

        for i in range(max_x):
            for j in range(max_y):
                min_dist = 1000000
                min_dist_idx = 0
                for k in range(len(coords)):
                    dist = distance.cityblock([i, j], coords[k])

                    if dist < min_dist:
                        min_dist = dist
                        min_dist_idx = k
                    elif dist == min_dist:
                        min_dist_idx = -1

                if min_dist_idx >= 0:
                    dists[min_dist_idx] = dists[min_dist_idx] + 1

        valid_coords = []

        for i in range(len(coords)):
            bounds = [0] * 4  # "boolean to determine if point is bounded"
            for j in range(len(coords)):
                # compare low X side
                if coords[j][0] < coords[i][0]:
                    bounds[0] = 1
                if coords[j][0] > coords[i][0]:
                    bounds[1] = 1
                if coords[j][1] < coords[i][1]:
                    bounds[2] = 1
                if coords[j][1] > coords[i][1]:
                    bounds[3] = 1

            if sum(bounds) == 4:
                valid_coords.append(i)

        max_area = 0
        for i in valid_coords:
            if dists[i] > max_area:
                max_area = dists[i]

        print valid_coords
        print dists
        return max_area


def day7_part1(input_file):

    input_list = []
    all_steps = []
    step_depends = {}

    with open(input_file) as file:
        all_lines = [line.rstrip() for line in file]

        for line in all_lines:
            step = line[36]
            depends = line[5]
            input_list.append([depends, step])
            if step not in all_steps:
                all_steps.append(step)
            if depends not in all_steps:
                all_steps.append(depends)

    for i in input_list:
        if i[1] not in step_depends:
            step_depends[i[1]] = [i[0]]
        else:
            step_depends[i[1]].append(i[0])

    all_steps = sorted(all_steps)

    ready_steps = []
    deps = [x[1] for x in input_list]
    for letter in all_steps:
        if letter not in deps:
            ready_steps.append(letter)

    executed_steps = []
    current_step = ready_steps[0]
    while True:
        if current_step not in executed_steps:
            executed_steps.append(current_step)
        ready_steps.remove(current_step)

        for i in input_list:
            if i[1] not in executed_steps:
                if set(step_depends[i[1]]).issubset(executed_steps):
                    if i[1] not in ready_steps:
                        ready_steps.append(i[1])

        ready_steps = sorted(ready_steps)

        if not ready_steps:
            break
        current_step = ready_steps[0]

    return ''.join(executed_steps)


def day7_part2_failed(input_file):
    input_list = []
    all_steps = []
    step_depends = {}

    with open(input_file) as file:
        all_lines = [line.rstrip() for line in file]

        for line in all_lines:
            step = line[36]
            depends = line[5]
            input_list.append([depends, step])
            if step not in all_steps:
                all_steps.append(step)
            if depends not in all_steps:
                all_steps.append(depends)

    for i in input_list:
        if i[1] not in step_depends:
            step_depends[i[1]] = [i[0]]
        else:
            step_depends[i[1]].append(i[0])

    all_steps = sorted(all_steps)

    step_values = {}

    for s in all_steps:
        step_values[s] = ord(s) - ord('A') + 61

    print step_values

    ready_steps = []
    deps = [x[1] for x in input_list]
    for letter in all_steps:
        if letter not in deps:
            ready_steps.append(letter)

    print ready_steps

    workers = 5
    time = 0
    execution_times = [0] * workers
    executed_steps = []

    while True:
        # print ready_steps
        finished = []

        for i in range(len(ready_steps)):
            if execution_times[i] > step_values[ready_steps[i]]:
                finished.append(ready_steps[i])
                executed_steps.append(ready_steps[i])
                execution_times[i] = 0

            execution_times[i] = execution_times[i] + 1

        ready_steps = [x for x in ready_steps if x not in finished]

        for i in input_list:
            if i[1] not in executed_steps:
                if set(step_depends[i[1]]).issubset(executed_steps):
                    if i[1] not in ready_steps:
                        ready_steps.append(i[1])

        if not ready_steps:
            break

        ready_steps = sorted(ready_steps)
        # print ready_steps

        time = time + 1

    return time


def day8_part1(input_file):

    numbers = []

    with open(input_file) as file:
        numbers = [int(x) for x in file.read().split()]

    # pre-seed the first item as "root"
    children = [numbers[0]]
    metas = [numbers[1]]
    meta_total = 0
    state = 0
    i = 2

    # managed to get the total without recursion!
    while True:
        # we're done
        if i >= len(numbers):
            break
        # state 0 == the first part of the header
        if state == 0:
            children.append(numbers[i])
            state = 1
        # state 1 == the second part of the header
        elif state == 1:
            metas.append(numbers[i])
            # if the previous node has more children, go back to state 0
            if children[-1] > 0:
                state = 0
            # if not, then we must be grabbing meta values next
            else:
                state = 2
        # state 2 == the state to grab meta values
        elif state == 2:
            # grab the meta values
            for x in range(metas[-1]):
                meta_total = meta_total + numbers[i + x]
            # now we're done w/this child
            children.pop()
            # we're also done w/the metas, make sure to add to the index
            i = i + metas.pop()
            # if there's no more children we're done
            if not children:
                break
            # decrement the amount of children left
            children[-1] = children[-1] - 1

            # if there's no more children, we're grabbing metas next
            if children[-1] == 0:
                state = 2
                continue
            else:
                state = 0
                continue

        i = i + 1

    return meta_total


def parse_next(numbers):
    # recursive function for creating the tree
    children = numbers[0]
    metas = numbers[1]
    # slice the numbers down since we just grabbed the header
    numbers = numbers[2:]
    values = []

    # recursively grab the children if there are any
    for i in range(children):
        value, numbers = parse_next(numbers)
        values.append(value)

    # if there aren't any children, we're grabbing metas
    if children == 0:
        return (sum(numbers[:metas]), numbers[metas:])
    else:
        # if there are children grab the sum of the values, given that the
        # meta values exist as sub elements
        total = sum(values[k - 1]
                    for k in numbers[:metas] if k > 0 and k <= len(values))

        # grab the result and pass it back up the tree
        return (total, numbers[metas:])


def day8_part2(input_file):

    numbers = []

    with open(input_file) as file:
        numbers = [int(x) for x in file.read().split()]

    # start the recursion, the return values are mainly to help
    # the recursion, so we don't care about the numbers array
    # that gets returned
    values, dontcare = parse_next(numbers)

    return values


def day9(player_count, last_marble_value):
    # blist here makes a massive difference in performance of python lists
    marbles = blist([0])
    current_marble = 0
    current_marble_idx = 0
    player_idx = 0
    scores = [0] * player_count

    while True:
        # get the next marble
        next_marble = current_marble + 1

        # if we're past the last marble, we're done
        if next_marble > last_marble_value:
            break

        # the special 23 case
        if next_marble % 23 == 0:
            # score the new 23 marble instead of inserting it
            scores[player_idx] = scores[player_idx] + next_marble

            # the next marble index is 7 to the left, then we have to give it
            # context of the length of the array so we don't go off the end
            next_marble_idx = (current_marble_idx - 7 +
                               len(marbles) - 1) % (len(marbles)) + 1

            # also score the marble we landed on and remove it
            scores[player_idx] = scores[player_idx] + marbles[next_marble_idx]
            del marbles[next_marble_idx]
        else:
            # insert the marble into the next index (also given in the context
            # of the length of the array)
            next_marble_idx = (current_marble_idx + 1) % len(marbles) + 1
            marbles.insert(next_marble_idx, next_marble)

        current_marble = next_marble
        current_marble_idx = next_marble_idx

        # keep track of whose turn it is
        player_idx = player_idx + 1
        if player_idx >= player_count:
            player_idx = 0

    return max(scores)


def print_message(positions):
    for x in range(130, 170):
        for y in range(110, 200):
            if [y, x] in positions:
                print '#',
            else:
                print '.',
        print '\n'
    print '---------------------------------'


def day10(input_file):

    positions = blist([])
    velocities = blist([])

    with open(input_file) as file:
        for line in file:
            # there's got to be a better way...
            p = line.split('<')[1].split('>')[0].split(',')
            v = line.split('<')[2].split('>')[0].split(',')

            # make the lists ints
            positions.append([int(p[0]), int(p[1])])
            velocities.append([int(v[0]), int(v[1])])

    count = len(positions)
    time = 0

    while True:
        # updated the positions each second
        for i in range(count):
            positions[i][0] = positions[i][0] + velocities[i][0]
            positions[i][1] = positions[i][1] + velocities[i][1]

        # calculate the distance between the points.  This calculation
        # isn't cheap, so I ran it only every 100 points or so at first
        # to try to narrow down the correct second to examine.  I'm sure
        # there's some way I could have detected straight lines but
        # this was easier.
        dist_total = 0

        if time > 10008:
            for i in range(count):
                for j in range(count):
                    dist_total = dist_total + \
                        distance.cityblock(positions[i], positions[j])

            print '{} {}'.format(time, dist_total)

        time = time + 1

        # once I got in the ballpark of the correct second, I printed it
        # out here.  I'm sure there's some way to convert this into
        # actual text but I didn't bother :)
        # if time > 10008:
        #     print time
        #     print_message(positions)
        if time == 10011:
            print_message(positions)
            print positions
            break


def get_power(x, y, serial):
    rack_id = x + 10
    return int((rack_id * (rack_id * y + serial) / 100)) % 10 - 5

# gets the power of a given square staring at point x, y for size size


def get_square(x, y, size, values):
    p = 0
    for x_value in range(x, x + size):
        p = p + sum(values[x_value - 1][y - 1:y - 1 + size])

    return p


def day11(serial):
    # using blist again because why not?
    values = blist([[0 for x in range(300)] for y in range(300)])

    # fill in the initial power values for the grid
    for x in range(1, 300):
        for y in range(1, 300):
            values[x - 1][y - 1] = get_power(x, y, int(serial))

    max_power = 0
    max_power_coords = []

    # here I'm manually grabbing a 3x3 square, made the first part simple, but
    # for the second part with the variable square size I had to change the approach
    # (using the get_square() function above)
    for x in range(1, 300 - 2):
        for y in range(1, 300 - 2):
            p = values[x - 1][y - 1] + values[x][y - 1] + values[x + 1][y - 1] + values[x - 1][y] + values[
                x][y] + values[x + 1][y] + values[x - 1][y + 1] + values[x][y + 1] + values[x + 1][y + 1]
            if p > max_power:
                max_power = p
                max_power_coords = [x, y]

    print 'part1: {}'.format(max_power_coords)

    # Part 2, this seemed like it was going to take forever, with each bigger square taking
    # a long time, so I decided to print the power and look for a pattern.  Sure enough
    # the correct answer given in the problem came out and then it would keep running
    # and never find a higher square.  So it ends up meaning no need to sum all 300 square
    # sizes.  There's surely a better method than this to truly calculate all of them,
    # but this worked.
    max_grid = 0
    max_grid_coords = []
    for i in range(1, 300):
        print i
        for x in range(1, 300 - i - 1):
            for y in range(1, 300 - i - 1):
                p = get_square(x, y, i, values)
                if p > max_grid:
                    max_grid = p
                    max_grid_coords = [x, y, i]
                    print '{} {}'.format(max_grid, max_grid_coords)


def day12(input_file):

    notes = []
    initial = ''

    with open(input_file) as file:
        initial = file.readline().replace('#', 'A').replace(
            '.', 'B').split(':')[1].strip()
        file.readline()

        for note in file.readlines():
            notes.append([x.strip() for x in note.replace('#', 'A').replace(
                '.', 'B').split('=>')])

    gens = []
    next_gen = initial
    prev_total = 0

    offset = 0
    for i in range(100):

        if next_gen[-5:] != 'BBBBB':
            next_gen = next_gen + 'BBBBB'
        if next_gen[:5] != 'BBBBB':
            next_gen = 'BBBBB' + next_gen
            offset = 5

        tmp_replacements = []

        for j in range(len(next_gen) - 5):
            for n in notes:
                if n[0] == next_gen[j:j + 5]:
                    tmp_replacements.append([j, n[1]])

        next_gen = 'B' * len(next_gen)
        for r in tmp_replacements:
            next_gen = next_gen[:r[0]] + r[1] + next_gen[r[0] + 1:]

        gens.append(next_gen)

        total = 0
        for j in range(len(next_gen)):
            if next_gen[j] == 'A':
                idx = (j - offset)
                total = total + idx

        print '{} {} {}'.format(i, total, total - prev_total)

        prev_total = total

    return total


def get_next_track_coord(x, y, ch):
    if ch == '^':
        return [x, y - 1]
    if ch == 'v':
        return [x, y + 1]
    if ch == '>':
        return [x + 1, y]
    if ch == '<':
        return [x - 1, y]


def get_missing_track(x, y, track):
    left = 0
    right = 0
    up = 0
    down = 0

    if x + 1 < len(track[0]):
        if track[y][x + 1] not in ['^', '<', 'v', '>', ' ']:
            right = 1
    if x - 1 >= 0:
        if track[y][x - 1] not in ['^', '<', 'v', '>', ' ']:
            left = 1
    if y + 1 < len(track):
        if track[y + 1][x] not in ['^', '<', 'v', '>', ' ']:
            down = 1
    if y - 1 >= 0:
        if track[y - 1][x] not in ['^', '<', 'v', '>', ' ']:
            up = 1

    if up and down and left and right:
        return '+'
    elif up and down and not left and not right:
        return '|'
    elif not up and not down and left and right:
        return '-'
    elif not up and down and not left and right:
        return '/'
    elif up and not down and left and not right:
        return '/'
    elif not up and down and left and not right:
        return '\\'
    elif up and not down and not left and right:
        return '\\'


def turn_left(ch):
    if ch == '^':
        return '<'
    if ch == '>':
        return '^'
    if ch == 'v':
        return '>'
    if ch == '<':
        return 'v'


def turn_right(ch):
    if ch == '^':
        return '>'
    if ch == '>':
        return 'v'
    if ch == 'v':
        return '<'
    if ch == '<':
        return '^'


def get_next_dir(cart):
    if cart[3] == 0:
        cart[3] = 1
        cart[0] = turn_left(cart[0])
    elif cart[3] == 1:
        cart[0] = cart[0]
        cart[3] = 2
    elif cart[3] == 2:
        cart[3] = 0
        cart[0] = turn_right(cart[0])


def print_track(track):
    for row in track:
        print ''.join(row)


def day13(input_file):
    track = []
    carts = []

    # cart layout:
    # 0 - character
    # 1 - (x, y) pos
    # 2 - stack to push/pop previous track values
    # 3 - turn state (0, 1, 2)

    x = 0
    y = 0
    with open(input_file) as file:
        for line in file:
            row = []
            x = 0
            for ch in line[:-1]:
                if ch in ['^', '<', 'v', '>']:
                    carts.append([ch, [x, y], [], 0, 0])
                row.append(ch)
                x = x + 1
            track.append(row)
            y = y + 1

    for cart in carts:
        ch = get_missing_track(cart[1][0], cart[1][1], track)
        cart[2].append(ch)

    tick = 0
    remove_carts = []
    try:
        while True:
            valid_cart_count = 0

            for rc in remove_carts:
                for c in carts:
                    if c[1] == rc[1]:
                        track[c[1][1]][c[1][0]] = c[2].pop()
                        c.pop()

            remove_carts = []

            # for c in carts:
            #     if c[1] in remove_carts:
            #         track[c[1][1]][c[1][0]] = c[2].pop()
            #         print track[c[1][1]][c[1][0]]
            #         c.pop()
            # remove_carts = []

            # print '{} {}'.format(tick, carts)
            # print_track(track)
            for cart in carts:
                if cart[4] == 1:
                    continue

                # print cart
                valid_cart_count = valid_cart_count + 1
                next_coord = get_next_track_coord(
                    cart[1][0], cart[1][1], cart[0])
                # print next_coord

                # if not next_coord:
                #     continue

                # if next_coord[1] >= len(track) or next_coord < 0:
                #     if next_coord[0] >= len(track[next_coord[1]]):
                #         continue

                print '{} {} {} {}'.format(next_coord, len(track), len(track[next_coord[1]]), cart)
                next_ch = track[next_coord[1]][next_coord[0]]
                # track[next_coord[1]][next_coord[0]] = cart[2].pop()
                track[cart[1][1]][cart[1][0]] = cart[2].pop()
                cart[2].append(next_ch)
                cart[1] = next_coord
                if next_ch == '+':
                    get_next_dir(cart)
                elif next_ch == '\\':
                    if cart[0] in ['>', '<']:
                        cart[0] = turn_right(cart[0])
                    else:
                        cart[0] = turn_left(cart[0])
                elif next_ch == '/':
                    if cart[0] in ['>', '<']:
                        cart[0] = turn_left(cart[0])
                    else:
                        cart[0] = turn_right(cart[0])

            for c1 in carts:
                if c1[4] == 1:
                    continue
                self_count = 0
                for c2 in carts:
                    if c2[4] == 1:
                        continue
                    if c1[1] == c2[1]:
                        if self_count == 1:
                            print 'hit a cart! {}'.format(c1[1])
                            c1[4] = 1
                            c2[4] = 1
                        else:
                            self_count = self_count + 1

            if valid_cart_count == 1:
                for c in carts:
                    if c[4] == 0:
                        print c
                print 'last cart!'
                raise Found
            tick = tick + 1
    except Found:
        pass


def get_digits(value):
    # unused function in failed day14 solution
    v = value
    digits = []
    while v > 0:
        digits.append(v % 10)
        v = v / 10

    return list(reversed(digits))


def combine_digits(digits):
    # unused function in failed day14 solution
    i = 0
    v = 0
    for d in reversed(digits):
        v = v + d * math.pow(10, i)
        i = i + 1
        print v

    return int(v)


def day14(recipe_count):
    elves = [0, 1]
    recipe = '37'
    recipe_count_str = str(recipe_count)
    recipe_count_str_len = len(recipe_count_str) + 1
    # digits = blist(get_digits(recipe))

    # while len(recipe) < recipe_count + 10:
    while recipe_count_str not in recipe[-recipe_count_str_len:]:
        val = 0
        for i in range(len(elves)):
            val = val + int(recipe[elves[i]])

        # digits = digits + blist(get_digits(val))
        recipe = recipe + str(val)

        for i in range(len(elves)):
            elves[i] = (elves[i] + (1 + int(recipe[elves[i]]))) % len(recipe)

    # print ''.join(str(x) for x in digits[recipe_count:recipe_count + 10])
    print 'day14Part1: {}'.format(recipe[recipe_count:recipe_count + 10])
    print 'day14Part2: {}'.format(recipe.index(recipe_count_str))

import copy


def deep_copy_params(to_call):
    def f(*args, **kwargs):
        return to_call(*(copy.deepcopy(x) for x in args),
                       **{k: copy.deepcopy(v) for k, v in kwargs.items()})
    return f


@deep_copy_params
def op_addr(i, r):
    r[i[3]] = r[i[1]] + r[i[2]]
    return r


@deep_copy_params
def op_addi(i, r):
    r[i[3]] = r[i[1]] + i[2]
    return r


@deep_copy_params
def op_mulr(i, r):
    r[i[3]] = r[i[1]] * r[i[2]]
    return r


@deep_copy_params
def op_muli(i, r):
    r[i[3]] = r[i[1]] * i[2]
    return r


@deep_copy_params
def op_banr(i, r):
    r[i[3]] = r[i[1]] & r[i[2]]
    return r


@deep_copy_params
def op_bani(i, r):
    r[i[3]] = r[i[1]] & i[2]
    return r


@deep_copy_params
def op_borr(i, r):
    r[i[3]] = r[i[1]] | r[i[2]]
    return r


@deep_copy_params
def op_bori(i, r):
    r[i[3]] = r[i[1]] | i[2]
    return r


@deep_copy_params
def op_setr(i, r):
    r[i[3]] = r[i[1]]
    return r


@deep_copy_params
def op_seti(i, r):
    r[i[3]] = i[1]
    return r


@deep_copy_params
def op_gtir(i, r):
    r[i[3]] = int(i[1] > r[i[2]])
    return r


@deep_copy_params
def op_gtri(i, r):
    r[i[3]] = int(r[i[1]] > i[2])
    return r


@deep_copy_params
def op_gtrr(i, r):
    r[i[3]] = int(r[i[1]] > r[i[2]])
    return r


@deep_copy_params
def op_eqir(i, r):
    r[i[3]] = int(i[1] == r[i[2]])
    return r


@deep_copy_params
def op_eqri(i, r):
    r[i[3]] = int(r[i[1]] == i[2])
    return r


@deep_copy_params
def op_eqrr(i, r):
    r[i[3]] = int(r[i[1]] == r[i[2]])
    return r


def num_matching_opcodes(before, instruction, after):
    x = 0

    opcodes = {}
    if after == op_addr(instruction, before):
        opcodes['op_addr'] = instruction[0]
        x = x + 1
    if after == op_addi(instruction, before):
        opcodes['op_addi'] = instruction[0]
        x = x + 1
    if after == op_mulr(instruction, before):
        opcodes['op_mulr'] = instruction[0]
        x = x + 1
    if after == op_muli(instruction, before):
        opcodes['op_muli'] = instruction[0]
        x = x + 1
    if after == op_banr(instruction, before):
        opcodes['op_banr'] = instruction[0]
        x = x + 1
    if after == op_bani(instruction, before):
        opcodes['op_bani'] = instruction[0]
        x = x + 1
    if after == op_borr(instruction, before):
        opcodes['op_borr'] = instruction[0]
        x = x + 1
    if after == op_bori(instruction, before):
        opcodes['op_bori'] = instruction[0]
        x = x + 1
    if after == op_setr(instruction, before):
        opcodes['op_setr'] = instruction[0]
        x = x + 1
    if after == op_seti(instruction, before):
        opcodes['op_seti'] = instruction[0]
        x = x + 1
    if after == op_gtir(instruction, before):
        opcodes['op_gtir'] = instruction[0]
        x = x + 1
    if after == op_gtri(instruction, before):
        opcodes['op_gtri'] = instruction[0]
        x = x + 1
    if after == op_gtrr(instruction, before):
        opcodes['op_gtrr'] = instruction[0]
        x = x + 1
    if after == op_eqir(instruction, before):
        opcodes['op_eqir'] = instruction[0]
        x = x + 1
    if after == op_eqri(instruction, before):
        opcodes['op_eqri'] = instruction[0]
        x = x + 1
    if after == op_eqrr(instruction, before):
        opcodes['op_eqrr'] = instruction[0]
        x = x + 1

    # print 'op_addr: {}'.format(op_addr(instruction, before))
    # print 'op_addi: {}'.format(op_addi(instruction, before))
    # print 'op_mulr: {}'.format(op_mulr(instruction, before))
    # print 'op_muli: {}'.format(op_muli(instruction, before))
    # print 'op_banr: {}'.format(op_banr(instruction, before))
    # print 'op_bani: {}'.format(op_bani(instruction, before))
    # print 'op_borr: {}'.format(op_borr(instruction, before))
    # print 'op_bori: {}'.format(op_bori(instruction, before))
    # print 'op_setr: {}'.format(op_setr(instruction, before))
    # print 'op_seti: {}'.format(op_seti(instruction, before))
    # print 'op_gtir: {}'.format(op_gtir(instruction, before))
    # print 'op_gtri: {}'.format(op_gtri(instruction, before))
    # print 'op_gtrr: {}'.format(op_gtrr(instruction, before))
    # print 'op_eqir: {}'.format(op_eqir(instruction, before))
    # print 'op_eqri: {}'.format(op_eqri(instruction, before))
    # print 'op_eqrr: {}'.format(op_eqrr(instruction, before))

    return x, opcodes

import ast


def day16(input_file):
    opcodes = {}
    opcodes['op_addr'] = []
    opcodes['op_addi'] = []
    opcodes['op_mulr'] = []
    opcodes['op_muli'] = []
    opcodes['op_banr'] = []
    opcodes['op_bani'] = []
    opcodes['op_borr'] = []
    opcodes['op_bori'] = []
    opcodes['op_setr'] = []
    opcodes['op_seti'] = []
    opcodes['op_gtir'] = []
    opcodes['op_gtri'] = []
    opcodes['op_gtrr'] = []
    opcodes['op_eqir'] = []
    opcodes['op_eqri'] = []
    opcodes['op_eqrr'] = []

# print num_matching_opcodes([3, 2, 1, 1], [9, 2, 1, 2], [3, 2, 2, 1])
    with open(input_file) as file:
        before_reg = []
        after_reg = []
        instruction = []
        state = 0
        match_count = 0

        for line in file:
            if 'Before' in line:
                before_reg = ast.literal_eval(line.split(':')[1].strip())
                state = 1

            elif 'After' in line:
                after_reg = ast.literal_eval(line.split(':')[1].strip())
                state = 0

                x, o = num_matching_opcodes(
                    before_reg, instruction, after_reg)
                if x >= 3:
                    match_count = match_count + 1

                for k, v in o.iteritems():
                    opcodes[k].append(v)

            elif state == 1:
                instruction = [int(x) for x in line.split()]
                state = 2

        print match_count
        for k, v in opcodes.iteritems():
            print '{} {}'.format(k, sorted(list(set(v))))

        # from here I manually parsed the list to get the actual opcodes


def print_grid(grid):
    i = 0
    for row in grid:
        print '{:04d} {}'.format(i, ''.join(row))
        i = i + 1


def expand(w_idx, row_idx, grid):
    if row_idx + 1 >= len(grid):
        return
    if grid[row_idx + 1][w_idx] == '#':
        if w_idx - 1 >= 0:
            expand(w_idx - 1, row_idx + 1, grid)
        if w_idx + 1 < len(grid[0]):
            expand(w_idx + 1, row_idx + 1, grid)

    grid[row_idx + 1][w_idx] = '|'


def get_bounds(num, bound_list):
    lower_bound = min(bound_list)
    upper_bound = max(bound_list)

    lower_bound_found = False
    upper_bound_found = False

    for n in bound_list:
        if n < num and n >= lower_bound:
            lower_bound = n
            lower_bound_found = True
        if n > num and n <= upper_bound:
            upper_bound = n
            upper_bound_found = True

    if lower_bound_found and upper_bound_found:
        return [lower_bound, upper_bound]
    else:
        return [-1, -1]


# def fill(water_idx, curr_row, next_row):
#     if curr_row[water_idx] not in ('#', '|', '+'):
#         curr_row[water_idx] = '|'

#     walls = [i for i, x in enumerate(next_row) if x == '#']

#     bounds = get_bounds(water_idx, walls)

#     print 'walls {} bounds {}'.format(walls, bounds)

#     if bounds is not [-1, -1]:
#         new_water_count = bounds[1] - bounds[0]
#         print new_water_count
#         # curr_row = curr_row[:bounds[0] - 1] + ['|'] * \
#         #     (new_water_count) + curr_row[bounds[1] + 1:]
#         curr_row = ['x'] * len(curr_row)

def fill(water_idx, curr_row, next_row):
    if curr_row[water_idx] not in ['#', '+', '~']:
        curr_row[water_idx] = '|'

    curr_walls = [i for i, x in enumerate(curr_row) if x == '#']
    if len(curr_walls) < 2:
        return -1

    bounds = get_bounds(water_idx, curr_walls)

    if bounds == [-1, -1]:
        return -1
    else:
        for i in range(bounds[0], bounds[1]):
            if next_row[i] not in ['#', '~', '.']:
                return -1

    total_fill = True
    for i in range(bounds[0], bounds[1]):
        if next_row[i] not in ['#', '~']:
            total_fill = False
            break

    # if we got here we have a fill
    if total_fill:
        for i in range(bounds[0] + 1, bounds[1] - 1):
            curr_row[i] = '~'

        curr_row[water_idx] = '~'
    else:
        next_walls = [i for i, x in enumerate(next_row) if x == '#']
        next_bounds = get_bounds(water_idx, next_walls)
        spill = False
        for i in range(bounds[0] + 1, bounds[1] - 1):
            if next_row[i] == '~':
                curr_row[i] = '|'
            elif next_row[i] == '#':
                curr_row[i] = '|'
                spill = True
            elif next_row[i] == '.' and spill == True:
                curr_row[i] = '|'
                return i

    return -1


def day17(input_file):
    grid = []
    clay_coords = []

    max_x = 0
    min_x = 100000000
    max_y = 0
    min_y = 0

    with open(input_file) as file:
        for line in file:
            raw = line.split(',')
            if 'x' in raw[0]:
                x = int(raw[0].split('=')[1])
                if x > max_x:
                    max_x = x
                if x < min_x:
                    min_x = x
                y_vals = raw[1].split('=')[1].split('..')
                for y in range(int(y_vals[0]), int(y_vals[1]) + 1):
                    if y > max_y:
                        max_y = y
                    clay_coords.append([x, y])
            else:
                y = int(raw[0].split('=')[1])
                if y > max_y:
                    max_y = y
                x_vals = raw[1].split('=')[1].split('..')
                for x in range(int(x_vals[0]), int(x_vals[1]) + 1):
                    if x > max_x:
                        max_x = x
                    if x < min_x:
                        min_x = x
                    clay_coords.append([x, y])

    print '{}, {} : {}, {}'.format(min_x, min_y, max_x, max_y)

    for y in range(max_y + 1):
        grid.append(['.'] * (max_x - min_x + 1))

    grid[0][500 - max_x - 1] = '+'

    for p in clay_coords:
        grid[p[1]][p[0] - min_x] = '#'

    # don't forget to order this each time!
    water_xs = [500 - min_x, 7]

    # curr_row = grid[0]
    # next_row = grid[1]

    water_idx = water_xs[0]

    curr_row = 0
    direction = 1

    # while True:
    #     ret = fill(water_idx, grid[curr_row], grid[curr_row + 1])
    #     if ret < 0:
    #         curr_row = curr_row + direction
    #     else:
    #         water_idx = ret
    #         curr_row = curr_row - 1

    # fill(water_idx, grid[0], grid[1])
    # fill(water_idx, grid[1], grid[2])
    # fill(water_idx, grid[2], grid[3])
    # fill(water_idx, grid[3], grid[4])
    # fill(water_idx, grid[4], grid[5])
    # fill(water_idx, grid[5], grid[6])
    # fill(water_idx, grid[6], grid[7])
    # fill(water_idx, grid[5], grid[6])
    # fill(water_idx, grid[4], grid[5])
    # fill(water_idx, grid[3], grid[4])
    # water_idx = fill(water_idx, grid[2], grid[3])
    # fill(water_idx, grid[3], grid[4])
    # fill(water_idx, grid[4], grid[5])
    # fill(water_idx, grid[5], grid[6])
    # fill(water_idx, grid[6], grid[7])
    # fill(water_idx, grid[7], grid[8])
    # fill(water_idx, grid[8], grid[9])
    # fill(water_idx, grid[9], grid[10])
    # fill(water_idx, grid[10], grid[11])
    # fill(water_idx, grid[11], grid[12])
    # fill(water_idx, grid[12], grid[13])
    # fill(water_idx, grid[13], grid[14])

# for row in range(0, max_y):
#     for w in range(len(water_xs)):
# expand(water_xs[w], row, grid)

# don't worry about rows w/no walls
# if '#' in grid[row]:
#     # see if there's a wall left of the first water stream
#     if grid[row].index('#') < water_xs[w]:
#         i = water_xs[w]
#         if grid[row][i] != '#':
#             grid[row][i] = '|'
#         while True:
#             i = i - 1
#             if grid[row][i] != '#':
#                 grid[row][i] = '|'
#             else:
#                 break
#             if i < 0:
#                 break
#         i = water_xs[w]
#         while True:
#             i = i + 1
#             if i >= max_x - min_x - 1:
#                 break
#             print i
#             if grid[row][i] != '#':
#                 if row + 1 < max_y:
#                     if grid[row + 1][i] == '#':
#                         grid[row][i] = '|'
#                     if i + 1 > max_x - min_x - 1:
#                         if grid[row + 1][i + 1] == '.':
#                             grid[row + 1][i + 1] = '|'
#                             break
#             else:
#                 break
#             if i > max_x - 1:
#                 break
#     else:
#         grid[row][water_xs[w]] = '|'

    fout = open('test.csv', 'w')

    for row in grid:
        for col in row:
            fout.write('{},'.format(col))
        fout.write('\n')

    fout.close()

    print_grid(grid)


def day16_part2(input_file):
    opcodes_actual = {}
    opcodes_actual[8] = 'op_borr'
    opcodes_actual[1] = 'op_bori'
    opcodes_actual[2] = 'op_addi'
    opcodes_actual[6] = 'op_addr'
    opcodes_actual[13] = 'op_muli'
    opcodes_actual[12] = 'op_mulr'
    opcodes_actual[4] = 'op_seti'
    opcodes_actual[7] = 'op_gtri'
    opcodes_actual[11] = 'op_eqir'
    opcodes_actual[5] = 'op_eqrr'
    opcodes_actual[0] = 'op_eqri'
    opcodes_actual[14] = 'op_gtrr'
    opcodes_actual[9] = 'op_gtir'
    opcodes_actual[10] = 'op_setr'
    opcodes_actual[3] = 'op_bani'
    opcodes_actual[15] = 'op_banr'

    reg = [0, 0, 0, 0]

    with open(input_file) as file:
        for line in file:
            instruction = [int(x) for x in line.split()]
            reg = globals()[opcodes_actual[instruction[0]]](instruction, reg)

    print reg


def addr(a, b, r):
    return r[a] + r[b]


def addi(a, b, r):
    return r[a] + b


def bani(a, b, r):
    return r[a] & b


def bori(a, b, r):
    return r[a] | b


def mulr(a, b, r):
    return r[a] * r[b]


def muli(a, b, r):
    return r[a] * b


def setr(a, b, r):
    return r[a]


def seti(a, b, r):
    return a


def gtir(a, b, r):
    return a > r[b]


def gtrr(a, b, r):
    return r[a] > r[b]


def eqri(a, b, r):
    return r[a] == b


def eqrr(a, b, r):
    return r[a] == r[b]


def day19(input_file):
    reg = [0, 0, 0, 0, 0, 0]

    with open(input_file) as f:
        content = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end
        # of each line
        content = [x.strip() for x in content]

        ip_bound = int(content[0].split()[1])
        ip_idx = 0
        instructions = []

        for line in content[1:]:

            instruction_raw = line.split()
            instruction = [instruction_raw[0], [
                int(x) for x in instruction_raw[1:4]]]

            instructions.append(instruction)

        # print len(instructions)
        # print instructions

        count = 0
        while True:
            if ip_idx >= len(instructions):
                break

            instruction = instructions[ip_idx][0]
            a, b, c = instructions[ip_idx][1]

            # print '{} {} {} {}'.format(instruction, a, b, c)

            reg[ip_bound] = ip_idx

            reg[c] = globals()[instruction](a, b, reg)

            ip_idx = reg[ip_bound] + 1

        print reg[0]

from functools import reduce


def day19part2():
    x = 10551311

    # awesome factors finding algorithm found here
    # https://stackoverflow.com/a/6800214/1544725
    print sum(set(reduce(list.__add__,
                         ([i, x // i] for i in range(1, int(x**0.5) + 1) if x % i == 0))))

    # print total
dirs = ['N', 'S', 'E', 'W']


def get_path(curr_path, re_path, chars_to_try):
    valid_paths = []

    if chars_to_try <= 0:
        return valid_paths

    if len(curr_path) > 20:
        return valid_paths

    for i in range(4):
        # print 'testing: {}'.format(curr_path + dirs[i])
        if re_path.match(curr_path + dirs[i]):
            print 'matched {}'.format(curr_path + dirs[i])
            valid_paths += get_path(curr_path +
                                    dirs[i], re_path, len(curr_path) + 6)
        else:
            valid_paths += get_path(curr_path +
                                    dirs[i], re_path, chars_to_try - 1)

    return valid_paths

import exrex

# attempted brute force solution

# def day20():
#     regex = '^NNNEESWSSENENNEESWSESWSW(WSEESWSESWSEENNESSENESSWWWSEESENEESESSENENNESESWSEEENESSEENWNENWWWNNESEEEESESEENESSWSWWWW(NN(ESEEWWNW|)N|SSWWSESWSWWSEEESSSENESSESESSWWSWNNE(E|NWWWWSSWNNWNNENWWWSWSSWWWSSSENNEEENE(N(W|N)|SESWSESWWNNWSSSSEEEN(WW|ESESWSWWN(E|WSWSWWWSWSWNNENEEEN(E|WWWNNN(NNNWSWSWWSWNNENWWSSSSSESWWWNNE(S|NWWNWWNNNNNNWSWNWSWWNENWWWSESWSWNWWWSWWSWSWNNNE(S|NWWSWSESSWNWNWNNNNNWWWSWWSWWWNENE(S|ENE(NENWNWNWNENESSESENNNW(S|NENWNWSSWWSWSWNWWNEEENENWWSWNWWWWSWWNWWNENNESSE(NNNEENWNENNWNEENESSWSSSSESWSWS(WNNEWSSE|)EENENNENESSSENENWNNWNWNEEEEESEENWNWNENESES(SSESSEENNNNESEEENENWNNWWNENEEESSEESSENENWNNEES(SSSWSEEEENWWNENNEENENWNWWWS(EESWWSS(NNEENWESWWSS|)|WWNENEENWWWSWSSWW(SESWENWN|)WWWWWWS(WSESWSS(WNW(WNENWNENWNNNWNENNWWS(WWNENENWWWS(E|WNNWSWNWSSESWSWNNNWWSSWWSSSWWWSESWSESWWWSESWWSSEESSESSESE(SWWNWNWN(E|NWWSWWWSESWWSSWSEESWSSENESEESESSWWSESSENNEEEENNWNE(EESSSW(NN|SSEEESEESSWNWSWNNWSWNWNNWSSSSSWSSEEN(W|ESSESWSESWWSWNNWNWWNNNE(SSEESE(N|S)|NWWNWWNEN(ESENESE(SWW|NNWNE)|WNWWSESWSSE(N|ESSWNWSWSSEEEESWWSEESWWSSSEEEEENNNN(ESSESWSSESSWSEESWWSSWWNWWNNWNNN(EESEE(NWES|)(SWSES(WWNNWN|S)|E)|WWNNWWWSWWNENENNWW(S(S|E)|NENNEE(SWSEESSE(SWWNNSSEEN|)NN|NWWW(NEENNENWWNEENE(SSSSSWN(SENNNNSSSSWN|)|NWWSWW(NENWNNNNESEENE(SESSS(WNNWSW(NWS|SE)|SEE(ESNW|)NWNENN(ESE|WS))|NNWSW(S|NNWW(SSENSWNN|)NEENE(SS|ENNENWWSSWNWSW(SEWN|)NNENNEES(WSNE|)ENEEEE(SWWWEEEN|)NE(SEEWWN|)NWWNENE(S|ENWWNNEEES(WW|ENNWWNEENWWNENNNENENESSWSWSES(W|ENE(SSWWEENN|)N(NNESE(SWEN|)NNEENWNNNWSSSWNWWS(ESEWNW|)WWWWWSES(ENEWSW|)WWNNNENEENNNWWNWNNWNWWSWNWNEEENESEEEESSSS(ENNEENWNENWNEEENNWSWNWNNNEEEEEESEESEEENENNESESEEENNWSWNNW(S|NNNWWWSESWS(EENNSSWW|)SSWW(SEWN|)WWNNNWNENEESSW(N|SEE(SWWEEN|)NNNNWNNWNEESEEEEENESSWS(SENEEENNNWW(SESWENWN|)NWWWWS(E|WNNWNWNNN(WWWWWSEESSEE(NNWSNESS|)SES(ESNW|)WWWWWWWNNWSWSSWSWNNENNENENN(WSWWN(WWWWWSEEEESWWSSESSE(SSSSSWNNWSWWSESSWSSSENNEEESE(SSWSES(SWS(ESSNNW|)WWWNWNNESESEN(E|NWN(WWWNNNWSWNWNENESEN(NNWSWNNENWWSWNWNW(SSESWSESWSSSSEESEESSWNWSSS(SSWNW(SSSEEE(NWWEES|)ESSWSSEESWWWSWW(NNE(NNE(SS|N(WWSSNNEE|)E)|S)|SSEESWSSENEEESWWSSESENN(W|ESEESWWSEES(ENNNNNN(ESSNNW|)WSW(SEWN|)NNW(SS|WNENN(E(NWNEE(N(ESENSWNW|)WN(WSW(W|NNEENW(N|W))|E)|S)|SS)|WSWW(NE|WW|SESW)))|WSWNWWN(WSWSESSWNWSW(NNNNE(SS|NE(S|NWN(W(NNN|SS)|E)|E))|SESSENESES(WWWW(SS|NN)|EE(E|NNW(S|WN(NNNESSENESSWW(EENNWSNESSWW|)|WW)))))|E))))|NNNE(NNWSNESS|)SS)|ENESENENNW(NNWWNWNW(S|NENENNWS(NESSWSNENNWS|))|S))|NNESENESEES(EEENWNE(NWWS(WNW(S|WNWWNW(SSEEWWNN|)NEEN(WW|ESEEE(NWWEES|)SEESWWWNW(WW|S)))|S)|ESSESWWSWWNE(WSEENEWSWWNE|))|WWW))|E)|EN(NWSNES|)E))|E)|NNN(WWW(SEEWWN|)N|ESSEENNENESE(SESWWNWS(NESEENSWWNWS|)|NNNWNWWW(WSESS(WWN(NN|E)|ENE(NWES|)E|SS)|NEEN(WNSE|)EES(ENESSW(S(EENSWW|)S|W)|W)))))|NNN(W|E(NEWS|)S))|E)|EESWSESSENE(NW|SEE))|ESEEESEENN(ESESSS(WN(WWWS(WNWN(E|W)|EE)|N)|SEESEENWNNENEEEEESSWNWWSW(N|W|SEE(ESWSSWSWWWW(SEESSEENEN(WWSNEE|)NESEENWNENENNW(NEENWNENESEEEEEENN(WWWWW(SEEEEWWWWN|)WWWWS(E|WNWWS(E|WNWWS(E|WNWW(W|SSSSENN(E|N))))|SS)|ESSSENNEEESENN(ESSEENWNEESEESWSEENEEENESESWSWNWWSESWWWWWN(EEE|WN(ENSW|)WWWSSSSENEN(NWSNES|)ESEEESSESSWSWNWWNNEE(NWWWSSSWWSWNNEN(ESNW|)WWWWWSSSENNESE(N|SWSWWWNWWWWWSWWWSSEESENN(ESSENENWN(W|EESSSEESESWWSSSWSESESWWWSSSSSSSSESSSWWSESWWSESWS(WNNWSWWNNE(S|ENWNWWWNEENESENESS(W|S(S|ENNNNE(SESWENWN|)NWWSWNWSWNNNWSWNNNEENNENESEENE(NE(NWNNNEE(SWSNEN|)NN(W(N|WWS(EE|WNWWSESWSESEE(NNWSNESS|)SWWWWWWSWWSSWWWNNESENNNESENNNWSWNNENNWSWWSESSWNWWSESEESWWWNWWNENWN(WWSSE(SWSESWSWSWWWNWSSWNWSSWNNWNNESENE(ENENNN(WSSWWNWN(EESNWW|)WNWWSSW(SSWWSEEESSWNWSW(SWWSESEEN(NESEESWWS(WSWS(WWW(NEEN(WNWSNESE|)E|S(WW|E))|E)|EEEN(NNW(W|NNNNN(WSNE|)E(NWNSES|)ES(W|EE))|ESSSSW(WNNESNWSSE|)SEENEENWWNNEN(ESEESESEESSESSSEESEEENEEESEESSSE(NNNN(E|WWNNNWSSWWNWWNWSWNW(NENNNNENEEN(WWWSWNWSWSSW(WNENWWSWN(NEEEENEEENEE(SWEN|)NWWNWSSW(ENNESEWNWSSW|)|WW)|SEES(S|ENNNWS))|EEEE(SWSESSSWNNWNNWSWSW(WSEEE(N|SE(N|S(WW(SEES|NWWSE)|EEEE(NW|SW))))|N)|NEN(W|E(SSWENN|)EN(W|EEE(E|S)))))|SSEEE(SWEN|)E))|E(EE|SWSSWWWNEENWNWWS(E|WSS(ENSW|)WNNNWWWNN(EES(ENEE(NWES|)S(WS|EENW)|W)|W(SSSEESE(N|SWW(WNEWSE|)S(SSS|E))|NWWNNWN(E|NWWSS(ENSW|)WNN(NE(NWNWWEESES|)EE|WWS(WWWSWW|E))))))))|W)))|W)|N)|NNWWN(WSNE|)EEE)|EN(W|N|ESE(SWWSSS(W|E(SWEN|)NE(NWES|)S)|N)))|S)|N)|NNNEEEN(EEEENESESW(SSENE(NNNWN(EEENNW(NENEESENENNEEEEN(ESESS(EENNNNWW(NE|SESS)|WNWWWSSSE(NNEWSS|)SWWNW(NENNSSWS|)SWWW(S|N(EE|N)))|WNWWS(E|WNWSSWS(E|WN(WS|NNES))))|S)|W)|E(SWSSE(N|SWW(NNW|SEEE))|E))|WWWSW(N|WSSWWNNES(NWSSEEWWNNES|)))|WWWNNE(EESWWEENWW|)NWWSWWSESEN(SWNWNEWSESEN|)))))|E)|E)|SSSWWN(WSW(N|S(ESES(WWNSEE|)EENWN(W|E)|WW))|E)))))|EENEESESSEESESWWSESSWNWNNW(N(EE|WNWN(WSNE|)E(ESNW|)N)|SSWSWWSESEE(N(E(NWES|)EEEEEEEEEESEEESENNNEEENWNEEEESESSEENWNENNNWWNWNEENWNNNWSSWS(WWSWNWNWSSWSESES(SSWNWWWSESS(ENNSSW|)WNWNNWWSESWWWNWWN(WSSEEWWNNE|)ENENNWSW(NNNEES(W|ENNESESWSSW(S(EEENESEN(ESEWNW|)NNWSW(WSNE|)NNNNW(NEESSES(ENNNNNESEENENWW(S|NNESEEEESWWSES(WWSSS(ENENWESWSW|)WNWWNEE(WWSEESNWWNEE|)|EEN(W|NENENESESWWSWSSSSESSS(EEENWNNESEEEESEESWWWNWWSSWSWNWSSESSEEESSENENWNEESSESWSSWN(N|WSWWN(NWSSSSEESSWNWSWWNNWWWWSEESSWSESWS(EENENWN(NN|EEESENESSWSS(WNNW(SWS(W|S|E)|N)|SSESEEENNNWWW(SESENSWNWN|)NEENWNEENWNEENNN(WSSWW(NENWWW|SS)|NNENNENWWNENNN(WWSS(ENSW|)WNWN(W(N|SS(E|W(S(WNSE|)E|N)))|E)|EEENWWWNNEEEEENEEESENEENWNWNNNWWWNWNWSWWWWSWNNNNWSWSSSSWNNNWNENNWWS(SSSW(SSSSWWS(WNNENES(NWSWSSNNENES|)|EEESENNN(ESSSENNNEEN(WNNNSSSE|)EENEESESEN(NWES|)ESSWWWWN(E|WWSWSES(EEN(WNSE|)EE(SWEN|)EEEE(NNNWSS|E)|WSWNNN)|N)|W(NNE(N|S)|S))|SSS)|NNNWNNNNWWNEEEEESSES(ENNEEESSS(WNW(NE|SWS)|ENNNEENWWNENWNEESSENESSEESSSSWWNENWN(W(SSWNWSSESE(S(EESEENEEENENWWNENNEENWWWSSWSSS(W(NNNNNNNWW(SEWN|)NWNNNNNWNWSWSSWSWNNWWSESWSEE(EENESE(NNNWSNESSS|)S|SWWSEESWS(WWW(NWNNWSWWNENNWSWNWSSWNNWWNWSSSEE(NWES|)SWSEE(SSWNWWNNWWNWNENE(SS|NWNWWSS(ENSW|)WWSESSWSWNWSSEEENEN(NN|ESSSEE(NWNEWSES|)SWSSESEESWWW(SEEEE(NNNNN(EESE(N|SWW(N|SES(WSNE|)E(ESNW|)N))|WW(NEWS|)S(SENSWN|)W)|S)|WWNWWNEEE(SEWN|)NNWWN(ENSW|)WWWWNNWNWWWNNWWWNWSSW(NNW(NEEEENESES(WW|EESENNWNW(S|NNNENEESENEESENESESES(EENEENWWW(NNESENNEEESENENNWSWWNENEEESENNWWWNNEENNWNNWWSWNWN(WWWW(SESSSWSESSWSWWNEN(E|W(NEWS|)WSSW(NWNEWSES|)SEEEESS(SW(SESNWN|)WNWW(NEEESNWWWS|)S(W(N|WWWWWN(WSS(WNWWSESWWS(WNN(WWSEWNEE|)E|ES(SWNSEN|)EEE(S|NW(NEWS|)W))|E)|EE))|E)|EENWNNESESENNNW(S|NEEE(SWSNEN|)NWNEN(E(SEWN|)N|WWNWNWSSSE(N|S(WSS|EN))))))|WWWSWNW(WW|S))|EEEEEESWSESENNESSSSSSSSW(NNNW(NENWESWS|)WW|SWWSSSSWWWSW(SESWS(WNNWSWW(EENESSNNWSWW|)|EEEENEENNNW(NENEN(WWSNEE|)ESSENEEESEENWNWWWNNEES(ENNWWNNNWNEENNENEN(WWSWWN(E|WW(SESES(ENSW|)WW(SESSW(N|SES(ENNSSW|)WSES(S|WW))|N)|WW))|ESSWSWSSEEN(W|NENENWNEEEEEEEESSSWNNWWSSSSEN(NN|EESWSWWSSSSSSWWSWNWWSW(S(WWWWSWN(NEWS|)WWW(N|SES(E(EEE(NEWS|)S|N)|WSSE(N|SSSS(SEWN|)WNNWWNE(NWES|)E)))|EE(N|ESEEENW(NEENEESSW(N|WSEEENNE(NNNNWSS(S|WWNW(S|NEE(S|NWN(WSNE|)EEN(W|E(SSWENN|)NWNENNNW(NEWS|)SS))))|SSSWWSESE(SWWSWSWWNNNNN(ESSE(SWSNEN|)(NN|E)|WSWWSESE(N|SWW(SEESES(WSSNNE|)ENESEEE(NNW(SWN|NE)|SWSSWS(WNSE|)SSSWSSSENEE(SSSWSWSSW(NNNN(WS(WNNNW(NEESSNNWWS|)WW|S)|E(ENSW|)S)|SWWSSSWSWNNWSSWNNN(NESEES|WW(WWW|SSSE(NN|SWWSSW(SSSSSWW(NENNSSWS|)SSSSSESSESEENNNNNEESENNENNESESESWW(N|WSESWSSESSSWWSWWSSWSESESSWNWNWNNWWWWSWWNEN(WWSSSEEESWSWSWNWWWWNENNWSWSSSSSS(WNNNWNNW(S(SSEWNN|)W|NEE(SS|NEN(WWSNEE|)EENE(N|SE(N|SWSSE(N|EE)))))|SESENNWNEEEN(EEEEN(ESSSSSEEESEESWSWWWNWN(EESEWNWW|)WNNNNWWSSWN(WSW(SEESEE(N(NN|W)|SESESSWSWNWWNWNN(EES(ES(W|E)|W)|NWSWSSSWNN(NNEWSS|)WSSSWNN(N|WSWWW(SEESWSSSWSSWSSESENN(EEEESWSWSSSESEESSESENNNW(NEENESSWSSSENNEESWSSEESS(WWWWWWWNN(ESENEES(EE|W)|NWNWWN(EE|WSSSESS(WNWNWWWSWS(EENESE|WWWWWNNWWWSWS(EEENWESWWW|)WWNENNE(S|NESENEES(W|ENESENESE(ENESEENNW(S|NENE(SEWN|)NN(NNEWSS|)WWS(WS(SWS(W(S|WWNNWWSS(ENSW|)WNWNWNEE(S|EEENENWWSWNNENWWSWNWSSS(ENESNWSW|)WNNWWNNNE(SSENEEEEENN(WWS(W|E)|ENEEESE(SWWSWW(NENEWSWS|)SSENESE(SWSSE(NENSWS|)SWW(SEWN|)N|NN)|NEN(W|N)))|NWNWN(WWWSSEE(NWES|)ESSSSWSEEESWWWWNNNENNWSWSSSSSWWNENWNWNWSWWSSESSSSWSESSESS(WNWNNWWSWNWNWWSSSEN(N|ESS(EEE(N(N|WW)|E)|WWWNNNWWNEENNEEEEEE(ENNWNNNWSWSESWS(WWNWSWNWWWNWSSWNWNENWNWWSESWWSSSESWWSSESEEEESS(EENESENNWWW(S|NENNN(WSWS(E|SWNNN(W(SSW(SEWN|)W|N(WNEWSE|)E)|E))|NEESWSE(WNENWWEESWSE|)))|WNWSWNWWNWNWNENWNWNEEE(SWEN|)NWNENWNEE(NNWWS(E|WSWNWSSWSES(ENNESS|WWWWSWSSESEESWSSWWNN(ESNW|)NWNNWNWSSESSESSWNWNNWWWSWNWWWSSENESEEENESSWS(WNWSWNWSWWWWWWNWSWWWWNENENWNWNEEENENEENEENWNENN(WN(WNNWSSWSEES(E|WSWNWNWNENWWSSSESWSWSW(NNNE(NNNW(SS|NENWNNNEESEE(NNWSNESS|)EESSWWWS(EESNWW|)WNN(WNSE|)EEE)|S)|SS(ENEE(SWEN|)ENEN(E(ENSW|)S|W(WSWENE|)N)|SSSESWSS(NNENWNSESWSS|))))|E)|ESSESESENESEENNW(NWSWNN(WWSESNWNEE|)NNEEESS(WNWSNESE|)SESSS(SSWNWSS(EE|WNNWWSWWWN(EN(ESNW|)N|WSSEE(EEENSWWW|)SSWNWWNW(NEWS|)WSESWW(NWES|)S(SWEN|)EEEE(NWES|)EES(WWW|E)))|ENNEEENWWNW(S|WNNNWNW(S|NWNENWN(W|EEESWSSEES(W|EESWSS(WNNSSE|)ENEESS(EENE(EEENENESENEENNWNEENWNENESENEEENWWWWN(WWS(WS(E|SSWSSWNWSS(WWWSE(SWWWW(NNENNENEENEENNENN(WN(E|WWNEENNNW(NEWS|)SSWNW(NEWS|)SSSSSSWWNENNWSWWSESSSE(SWSSWNNWWWS(SENESSSEN(SWNNNWESSSEN|)|W(NNENWNWSWWWNEEN(ENENWNEENWNEEE(EESEE(NWES|)SWWSWNWSW(NNEEWWSS|)SESSSS(ENNSSW|)WNNNW(NN|S|W)|NN(NNE(S|NNNWN(EESESESWSWNN(SSENENSWSWNN|)|WSW(SEESNWWN|)NWSW(W|NNENN(WSNE|)E(SSEEWWNN|)N(E|W))))|WWWWS(EEE|W(SESNWN|)NNWW(SEWN|)N)))|W)|W))|NEE(SWEN|)ENE(S|N(NWSNES|)E)))|ES(E|SSSW(N|SWWSWWS(WSSWENNE|)EE)))|S)|E)|EEE))|E)|EEEEESSEESENESENENWWNWSWNW(NWN(NWSWWS(EE|W)|EEENESENEEESWWSW(SEESE(NNWESS|)SWSWSWSSWWWSSWNWWSWSEE(N|ESWSWWWSS(WNNW(S|WNN(NEEE(NNNWN(EESENEE(NWWWWEEEES|)S(WS(WWSNEE|)E|EEENWW)|WSSWSEEN(SWWNENSWSEEN|))|SS(WWNEWSEE|)EE)|WSW(SEWN|)N))|EEN(W|ESS(WW|EEE(S|NNNW(SSWNNSSENN|)NNESENEENN(EEESSS(ENNNEN(WNWNW(SW(WSEEEWWWNE|)N|NEE(NWES|)SE(S|N))|ESSWSE(SWSESNWNEN|)E)|WW(NNESNWSS|)WS(W(WSNE|)N|EE))|WWS(WNSE|)E))))))|WWW))|S))|SSWWWSEESWWWWWSE(E|S))|W(N|W)))))))|S))|EE(N|EEEEEEEEENWWN(WSNE|)ENWNEN(WNWW(SE|NEEES|W)|ESSSESSE(EE|N))))))|S))|EE)|SSWWN(WWW|E))))|ENEENEEE(SSWNWSWW(EENESEWNWSWW|)|NNWSWNWNNWWN(W(SSEESS(E|W(NWS|SE))|NENNW(W|S))|EEEEESWS(WNSE|)EEE(SWSSNNEN|)ENE(NWWSWNN(EEE|WW)|ES(E|W)))))|ENES(S|ENNWWN(W(SS|W)|EEN(WW|N)))))))|E)|E)|E))|SWWS(WWNEWSEE|)E))))|ENN(NW|ESS))))|EENWNEENWWW(W|NEENNWW(SEWN|)NENNWWNNEES(W|ENESSWSSENESSWSES(W|EENENWW(S|NNEENWWWNNENEENWWWSWNNWNEENNWNNNN(EESESWW(SEESWSSEES(WWW|ESSSWWS(W|EEEEESESWWWSSW(N(NNEEWWSS|)WW|SSSW(N|WSEESS(WNWSWNW(NNES|SWN)|ENNESSENNEEESES(WWNWSNESEE|)ENNWNENWNNWWWSWNW(NEEEENNEE(SS(WNSE|)SS|NNWWS(E|W(SS|WNENWWSWNW(S|NENEENEEESSS(WNNWSWW(EENESSNNWSWW|)|ENNNNWWWNWSWWNWWW(S(W|ESE(EE|N|S))|NENNNNENWW(NEEEESSW(SSESES(EESEENWNNWNWS(SEWN|)WN(W|NNEEENWNENWWSW(SEWN|)NWWS(E|WNWNNENEESWS(W|EEEENWWNNENENWWSWNNEENESENE(NWWNNENWNENE(SSSSWENNNN|)NWWNEENNWWW(WSW(S(ESSE(NNNEEWWSSS|)(SWW(SEESWSWWN(E|WSSSE(EEN(ENSW|)WW|SWW(SSENESS(WWSWNWWSESESSS(WWNWWNNNNW(NW(W|NNNEESS(WNSE|)ES(E|SSSES(ESNW|)W|W))|SSSS)|E)|E)|NWNENW)))|NN)|E)|W)|N)|NNNNNEESE(SSSWWNENWN(SESWSEWNENWN|)|NNWNNE(S|NNNNNNNWW(NEENWNEN(SWSESWENWNEN|)|SSWSEE(SWSWNWSSWWNN(E(NENNENW(ESWSSWENNENW|)|S)|WSWW(NEWS|)WSSW(NN|SSW(SEEESWSE(ENNENWNW(SWNSEN|)NEN(ESSENEEENE(ENSW|)SSWWWSS(WNSE|)SE(NNEEWWSS|)S|W)|SWWWWN(ENESNWSW|)WSSEESSW(SES|NW))|NN)))|NN)))))|SSWSSE(SSS(WNNW(N|W)|SSSSSW(SESSNNWN|)WW)|N)))))|W(S|WWN(E|NN)))|N)|SWSESS)))))))|SWSES(ENESEENNWS(NESSWWEENNWS|)|W)))))))|N)|WSSSW(N|SS(E(E|N)|SSWSES(ENSW|)WWWSWSS(E(SSE(SW|NE)|N)|WNWSSWWNW(S(SE|WN)|NEE(NENNES(S|ENNWWNNWSSSSW(S|NWWWWWNENEES(W|ENE(NWNN(WW(N|SESWWN)|ESENEEN(ESSEN(EESWSS(ENSW|)W(SSENSWNN|)WN(W(NWES|)S|E)|N)|W(N(E|NN)|WW)))|S))))|S)))))))))))|S)|W)|WNN(WSNE|)ENE(NWES|)SS(W|E)))))|N)|N)|W)|WN(WWSEWNEE|)E))|ENESENNN(WSWNNE(E|NNW(SWNSEN|)NN)|ESESS(WNSE|)ENNEN(WW|NNNEN(WNSE|)ESSWSEESWWS(NEENWWEESWWS|)))))|NNW(NEEWWS|)S)))))|NW(NENNN(WSSNNE|)NN|W)))|NW(S|NNN(NWES|)EE))))|N)))|W)))|NNWNNEENESESWS(WNWESE|)EENNNWNNNWW(SWWWS(WNNSSE|)EE(SW|EEN)|NEEN(W|ENWNEESSSWSSSE(NN|SSSS)))))))|W)|SWS(E|WW(SE|NE))))|NN(EEEN|WNE))))|S)|WWNWWW(N|SWSSE(SWW(NNWNN(WSNE|)(E(E|S)|N)|SES(W(WWWNSEEE|)S|E(N|E)))|NENESSW(ENNWSWENESSW|))))))|SS)|SEEEE(NWWEES|)SEESE(ESWWWSWWNEN(E|WNWSWSESWWWNEN(NEWS|)WWWSSE(N|SSSESSSENEENWNNN(EESS(WNSE|)SESWSSEEENNW(SWEN|)NENNWSWNNEENEEESESWSEE(NNE(NWWEES|)EE|SESWWSSE(EN(W|E(S|NN))|SS(WNWSWWWWSS(EENWESWW|)WNWNEN(EEEENENNN(E|WW(NNNESS|SS(ENSW|)S))|WWWN(ENWESW|)WSSWNNW(SSSES(WW(NN|W)|ENENESSWSW(W|SSENE(NE(EE|N)|SS)))|NNNWNNN(SSSESSNNWNNN|)))|SS)))|WWSESS)))|N)))))|EN(W|E(SEEEWWWN|)N))|S)|E))|W(WW|S))|E)|WSWNNWSSWNN(SSENNEWSSWNN|))|N)|N)|E))|WWW(NENWESWS|)S))|E)))))|WWNWWWNNEENE(NWWSWWNNE(NWN(NENWNEEENE(NWES|)SE(N|SWSWSSE(SWWNN(NNEWSS|)W|EEENENESS(W|E(NNNN(EE|W(SWWSW(SW(N|W)|N)|NN))|SS))))|WWSWSSWWNNNE(SS|N(ESNW|)WWWN(WSW(N|SSEESWWSES(WWNNSSEE|)EE(EEEESWWSWSW(SEENES(S(S|W)|ENESE(NNW(WW|N(E|NNN(WSNE|)N))|ES(W|SS)))|NN(E|WW))|N(NNNWWEESSS|)W))|E)))|S)|SS(E(NNEWSS|)S|WW)))|E))|W))))|W)|S)|S(W|S))|N))|S)|EEENN(NESSES(EESWENWW|)W|WW(SE|WN)))|E)|W)|SSSSEESWWWSEESSSWSWWSSEEESWWWSEESEESSSSWNNWWN(EE|NWWSSSE(NN|ESWWSWS(WNNWNENE(NWWSWWNWSSSEE(NWES|)SS(ENSW|)WWWSWWWNENE(E(EE|NNWNWWSS(WWNENWWSWNNENEEES(EENE(SS|NWNEEEEEENWNENESE(SWSESWSWNWWS(WNWSNESE|)(S|E)|NNNNE(SSENNES(NWSSWNSENNES|)|NWWWSSWWWNNNNWWNWSWSEESE(SWWWSSSSWNWSSWWSS(ENEEE(S(WW|S)|N(W|ENNNEN(W|ESESWWSSEEN(E(S|NEN(EENNSSWW|)W)|W))))|WSWWNNNENE(SSWSNENN|)NEE(SWEN|)NENNNE(SSS|EE|NWNWW(NEEE(ENEEN(EESWSEENEESSEES(ENNWWNEEE(NWES|)SS|WWWN(WSSESW|N))|WNN(ES|WNNW))|S)|SESWWWSWWN(E|WWSSS(EESW(W|SE(SSSW(SSE(N|EESWSESW(WWW(NNEESW|W)|SSEEN(W|NESESE(NEWS|)S(WW(N|WWSW(N|S))|SE(EEEEN|SW)))))|NN)|ENENEN(E(ENWWEESW|)S|WW(WWNSEE|)S)))|WNNNNW(SSSSW(SS|N|WWW(NN|W))|N(EEES(ENSW|)W|NW(SS|W))))))))|N))))|WW)|E(E|N)))|S)|SS)|ES(W|EEESSSSESWSEESENNNENWW(NNW(SS|NNWWN(WSNE|)E(EESSEEEENWNNESEESWSSSEENWNENNNNWW(SEWN|)WWNNWSSW(NNNNW(W|NENNN(ESSSEEN(WNNSSE|)ESSWS(WNWSNESE|)SEEN(EENWN(WSNE|)E(ESSEEE(N|SEESSEES(ENNN(WW(SEWN|)N(E|WW)|EE)|WWSWWNENWWSSWSSENEES(WSWWSESWWW(SWNWWS(ESWSNENW|)WWNNWWNEEESS(NNWWWSNEEESS|)|NNE(S|NWNNNNEE(SWSSNNEN|)NN(ESEEWWNW|)WSWNWSWWW(EEENESNWSWWW|)))|ENE(N|SEEESWSS(E(EE|N)|W)))))|N)|W)|W(WS(ESNW|)WW|NNE(NNWSNESS|)S)))|SSE(SWEN|)N)|NN))|SS)))))))))|WW))|S(E|W)))|WWWW))|WS(SWSNEN|)E)|NNEEE(ENW|SWW))|N)))|W(S|WWW))))|WWN(E|WW(SEWN|)W))))|WNWNENWW(EESWSEWNENWW|))|W)))))))|SSSSEN))|SS))))|WSSW(NNN(EE|NNNNW(SSWWWEEENN|)N)|WS(WNSE|)EE)))))))|NWWNWS(SESWSEE(WWNENWESWSEE|)|WNNNESENN(ESNW|)NWWS(E|WNW(NNEE(N|S(W|E))|WS(WNSE|)ESESSWNWN(SESENNSSWNWN|))))))|ENN(E|W(WNNWNN(NNESSSESE(SWEN|)NEENEENESEEEENWWWNNNWNEEESSS(WNNSSE|)EESE(ENWNNNWSSWNNNWNEEEE(S(SSE(NN|S(SS|W))|WW)|NWWWWWN(EENWWN|WSWSS(EENWESWW|)WNWSWNNN(WSSSSSESWWNW(NENWESWS|)SSESE(SWWNSEEN|)NEE(SWEN|)NNE(NWW|SSEN)|E(SEWN|)NN)))|SWSWS(E|WWN(E|WWS(E|SSS(SWNNSSEN|)E))))|WW)|S))))|E)|SSES(W|SS))|ENEESSWN(SENNWWEESSWN|))|E))|W)|W)|S))|S))))|EEE(SSWNWS|NN))))))))|N)$'
#     #re_path = re.compile(regex)

#     matches = list(exrex.generate(regex))
#     print len(max(matches, key=len))
#     # curr_path = ''
#     # valid_paths = []

#     # valid_paths += get_path('ENNWSWW', re_path, 12)

#     # print valid_paths

from collections import *


def day20(input_file):
    waypoints = []
    regex = [ch for ch in open(input_file).read(
    ) if ch != '\n' if ch != '^' if ch != '$']

    # easier way to convert the directions to x, y locations
    dirs = {'N': (0, -1), 'S': (0, 1), 'E': (1, 0), 'W': (-1, 0)}

    # start way out in the middle of nowhere
    current_x = 10000
    current_y = 10000

    prev_x = current_y
    prev_y = current_y
    path_lengths = defaultdict(int)

    for c in regex:
        if c == '(':
            # "push" onto the stack
            waypoints.append((current_x, current_y))
        elif c == ')':
            # pop off the stack
            current_x, current_y = waypoints.pop()
        elif c == '|':
            # don't care here so just skip it
            current_x, current_y = waypoints[-1]
        else:
            # update the position
            current_x += dirs[c][0]
            current_y += dirs[c][1]

            # calculate the distance
            if path_lengths[(current_x, current_y)] > 0:
                # only keep track of the shorter of the two paths to compare
                path_lengths[(current_x, current_y)] = min(
                    path_lengths[(current_x, current_y)], path_lengths[(prev_x, prev_y)] + 1)
            else:
                # comparison path didn't exist so just move on
                path_lengths[(current_x, current_y)] = path_lengths[
                    (prev_x, prev_y)] + 1

        prev_x = current_x
        prev_y = current_y

    print 'part1: {}'.format(max(path_lengths.values()))

    # print len(path_lengths)
    # print path_lengths

    count = 0
    for p in path_lengths.values():
        if p >= 1000:
            count += 1

    print 'part2: {}'.format(count)


def day21():
    # I started off attempting to directly translate
    # the register numbers then realized much of the
    # code "reduced" nicely to not need the other
    # registers.  The code was derived using the same
    # method as day 19.  It will roll and then eventaully
    # actually halt!
    r1 = 0
    nums = []

    while True:
        r2 = r1 | 65536
        r1 = 10605201

        while True:
            r1 = r1 + (r2 & 255)
            r1 = r1 & 16777215
            r1 = r1 * 65899
            r1 = r1 & 16777215

            if 256 > r2:
                if r1 not in nums:
                    print r1
                nums.append(r1)
                break

            # basically here we're checking every bit
            r2 = r2 // 256

    print nums


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("inputdir", help="directory of input files")
    args = parser.parse_args()

    # print 'day1Part1: {}'.format(day1_part1(args.inputdir + '/day01'))
    # print 'day1Part2: {}'.format(day1_part2_fast(args.inputdir + '/day01'))
    # print 'day2Part1: {}'.format(day2_part1(args.inputdir + '/day02'))
    # print 'day2Part2: {}'.format(day2_part2(args.inputdir + '/day02'))
    # print 'day3Part1: {}'.format(day3_part1(args.inputdir + '/day03'))
    # print 'day3Part2: {}'.format(day3_part2(args.inputdir + '/day03'))
    # print 'day4Part1: {}\nday4Part2: {}'.format(*day4_all(args.inputdir +
    #                                                       '/day04'))
    # print 'day5Part1: {}'.format(day5_part1_fast(args.inputdir + '/day05'))
    # print 'day5Part2: {}'.format(day5_part2_fast(args.inputdir + '/day05'))
    # print 'day7Part1: {}'.format(day7_part1(args.inputdir + '/day07'))
    # print 'day8Part1: {}'.format(day8_part1(args.inputdir + '/day08'))
    # print 'day8Part2: {}'.format(day8_part2(args.inputdir + '/day08'))
    # print 'day9Part1: {}'.format(day9_part1(418, 71339))
    # print 'day9Part2: {}'.format(day9(418, 7133900))
    # print 'day10: {}'.format(day10(args.inputdir + '/day10'))
    # print 'day11: {}'.format(day11(args.inputdir))
    # print 'day12: {}'.format(day12(args.inputdir + '/day12'))
    # print 'day13: {}'.format(day13(args.inputdir + '/day13'))
    # day14(5)
    # day14(681901)
    # day16(args.inputdir + '/day16')
    # day16_part2(args.inputdir + '/day16')
    # day17(args.inputdir + '/day17')
    # day19(args.inputdir + '/day19')
    # day19part2()
    # day20(args.inputdir + '/day20')
    day21()
