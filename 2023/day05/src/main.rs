use std::fs;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

#[derive(Debug)]
struct Mapping {
    source: u64,
    dest: u64,
    range: u64,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let mut seeds: Vec<u64> = Vec::new();
    let mut maps: [Vec<Mapping>; 7] = [vec![], vec![], vec![], vec![], vec![], vec![], vec![]];
    let mut map_index = 0;
    let mut first_map: bool = true;

    for line in data.lines() {
        // println!("{}", line);
        if line.contains("seeds") {
            let split: Vec<_> = line.split(':').collect();
            let seeds_split: Vec<_> = split[1]
                .trim()
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse().unwrap())
                .collect();
            for i in 0..seeds_split.len() / 2 {
                for j in 0..seeds_split[i * 2 + 1] {
                    seeds.push(seeds_split[i * 2] + j);
                }
            }
            // println!("{:?}", seeds);
        } else if line.contains(":") {
            if !first_map {
                map_index = map_index + 1;
            }
            first_map = false;
        } else {
            let map_split: Vec<_> = line
                .trim()
                .split(' ')
                .filter(|x| !x.is_empty())
                .map(|x| x.parse().unwrap())
                .collect();
            if map_split.len() > 0 {
                let m = Mapping { source: map_split[1], dest: map_split[0], range: map_split[2] };
                maps[map_index].push(m);
            }
        }
    }

    let mut minimum_location = u64::MAX;

    for seed in seeds {
        let mut current_seed = seed;
        // println!("current_seed: {}", current_seed);
        for i in 0..maps.len() {
            // println!("current set: {}", i);
            for map in maps[i].iter() {
                if current_seed >= map.source && current_seed < map.source + map.range {
                    // println!("fit into map {:?}", map);
                    current_seed = current_seed + map.dest - map.source;
                    // println!("new num: {}", current_seed);
                    break;
                }
            }
        }
        // println!("------");
        if current_seed < minimum_location {
            minimum_location = current_seed;
        }
    }

    println!("{}", minimum_location);
}
