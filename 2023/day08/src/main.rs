use clap::Parser;
use regex::Regex;
use std::collections::HashMap;
use std::fs;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let re_line = Regex::new(r"([A-Z]{3})").unwrap();
    let mut map: HashMap<String, [String; 2]> = HashMap::new();
    let mut keys: Vec<String> = Vec::new();
    let mut instructions: Vec<usize> = Vec::new();
    let mut first = true;

    for line in data.lines() {
        if first {
            let letters: Vec<char> = line.chars().collect();

            for letter in letters.iter() {
                if *letter == 'L' {
                    instructions.push(0);
                } else {
                    instructions.push(1);
                }
            }
            first = false;
        } else if !line.is_empty() {
            let words: Vec<String> = re_line
                .find_iter(line)
                .map(|x| x.as_str().to_string())
                .collect();
            map.insert(words[0].clone(), [words[1].clone(), words[2].clone()]);
            keys.push(words[0].clone());
        }
    }

    let mut ins = "AAA";
    let mut ins_index = 0;
    let mut step_count = 1;
    let mut ins_key;

    loop {
        ins_key = keys
            .iter()
            .position(|i| i == ins)
            .unwrap();

        ins = &map[&keys[ins_key]][instructions[ins_index]];

        if ins == "ZZZ" {
            break;
        }

        ins_index = ins_index + 1;
        if ins_index >= instructions.len() {
            ins_index = 0;
        }

        step_count = step_count + 1;
    }

    println!("{}", step_count);
}
