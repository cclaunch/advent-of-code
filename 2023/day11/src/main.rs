use std::fs;
use clap::Parser;
use std::collections::HashMap;
use itertools::Itertools;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    let width = data.find("\n").unwrap();
    let height =
        (data.len() -
            data
                .as_bytes()
                .iter()
                .filter(|&&c| c == b'\n')
                .count()) /
        width;

    let mut grid = vec![vec![0 as u32; width]; height];
    let mut galaxies = HashMap::new();
    let mut x = 0;
    let mut y = 0;
    let mut galaxy_num = 1;

    for line in data.lines() {
        let letters: Vec<char> = line.chars().collect();

        for letter in letters.iter() {
            if *letter == '#' {
                grid[x][y] = galaxy_num;

                galaxy_num = galaxy_num + 1;
            }

            y = y + 1;
        }
        x = x + 1;
        y = 0;
    }

    for row in grid {
        prinln!("{:?}", row);
    }
}
