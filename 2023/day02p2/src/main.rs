use clap::Parser;
use std::fs;
use regex::Regex;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let mut games_power_sum = 0;
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let re_line = Regex::new(r"(?:Game\s*(?P<game_id>-?\d+)+):(?P<draws>.*)").unwrap();
    let re_colors = Regex::new(
        r"(?P<blue>\d*)(?:\s*blue)|(?P<green>\d*)(?:\s*green)|(?P<red>\d*)(?:\s*red)"
    ).unwrap();

    for line in data.lines() {
        let Some(game) = re_line.captures(line) else {
            return;
        };

        let mut red_max = 0;
        let mut green_max = 0;
        let mut blue_max = 0;

        for (color, [count]) in re_colors.captures_iter(&game["draws"]).map(|c| c.extract()) {
            let x = count.parse::<u32>().unwrap();
            if color.contains("red") && x > red_max {
                red_max = x;
            } else if color.contains("green") && x > green_max {
                green_max = x;
            } else if color.contains("blue") && x > blue_max {
                blue_max = x;
            }
        }

        let game_power = red_max * green_max * blue_max;
        games_power_sum = games_power_sum + game_power;
    }

    println!("game power sum: {}", games_power_sum);
}
