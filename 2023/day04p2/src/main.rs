use std::fs;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    let width = data.find("\n").unwrap();
    let total_games =
        (data.len() -
            data
                .as_bytes()
                .iter()
                .filter(|&&c| c == b'\n')
                .count()) /
        width;

    let mut card_count = vec![1; total_games];
    let mut current_game = 0;
    let mut prev_win_count = 0;

    for line in data.lines() {
        let split: Vec<_> = line.split(':').collect();
        let game: Vec<_> = split[1].split('|').collect();
        let mut winning_nums: Vec<u32> = game[0]
            .trim()
            .split(' ')
            .filter(|x| !x.is_empty())
            .map(|x| x.parse().unwrap())
            .collect();
        let mut our_nums: Vec<u32> = game[1]
            .trim()
            .split(' ')
            .filter(|x| !x.is_empty())
            .map(|x| x.parse().unwrap())
            .collect();

        winning_nums.sort();
        our_nums.sort();

        let mut matches = 0;
        for winning_num in winning_nums {
            if our_nums.contains(&winning_num) {
                matches = matches + 1;
            }
        }

        if matches > 0 {
            for i in 1..matches + 1 {
                card_count[current_game + i] =
                    card_count[current_game + i] + card_count[current_game];
            }
            prev_win_count = prev_win_count + 1;
        }

        current_game = current_game + 1;
    }

    let total: u32 = card_count.iter().sum();
    println!("total: {}", total);
}
