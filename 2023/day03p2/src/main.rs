use std::fs;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    let width = data.find("\n").unwrap();
    let height =
        (data.len() -
            data
                .as_bytes()
                .iter()
                .filter(|&&c| c == b'\n')
                .count()) /
        width;
    let mut grid = vec![vec![-1 as i8; width]; height];
    let mut symbols: Vec<(usize, usize)> = Vec::new();

    let mut x = 0;
    let mut y = 0;

    for line in data.lines() {
        let letters: Vec<char> = line.chars().collect();

        for letter in letters.iter() {
            if letter.is_digit(10) {
                grid[x][y] = letter.to_digit(10).unwrap() as i8;
            } else if *letter == '.' {
                grid[x][y] = -2;
            } else if *letter == '*' {
                symbols.push((x, y));
            }
            y = y + 1;
        }
        x = x + 1;
        y = 0;
    }

    let mut symbols_w_nums: Vec<(u32, (usize, usize))> = Vec::new();

    for n in 0..height {
        let mut numvec: Vec<u32> = Vec::new();
        for m in 0..width {
            if grid[n][m] >= 0 {
                numvec.push(grid[n][m].try_into().unwrap());
            }
            if (grid[n][m] < 0 || m == width - 1) && numvec.len() > 0 {
                for x in m - numvec.len()..m {
                    let adjacent_symbols = is_adjacent(n, x, symbols.clone());
                    if adjacent_symbols.len() > 0 {
                        let mut partnum = 0;
                        for i in (0..numvec.len()).rev() {
                            partnum =
                                partnum +
                                (10 as u32).pow((numvec.len() - i - 1).try_into().unwrap()) *
                                    numvec[i];
                        }
                        for (x, y) in adjacent_symbols {
                            symbols_w_nums.push((partnum, (x, y)));
                        }
                        break;
                    }
                }
                numvec.clear();
            }
        }
    }

    let mut total = 0;

    for (sx, sy) in symbols {
        let mut gear_ratio = 0;
        for (num, (x, y)) in &symbols_w_nums {
            if *x == sx && *y == sy {
                if gear_ratio > 0 {
                    gear_ratio = gear_ratio * num;
                    total = total + gear_ratio;
                } else {
                    gear_ratio = *num;
                }
            }
        }
    }

    println!("{}", total);
}

fn is_adjacent(x: usize, y: usize, symbols: Vec<(usize, usize)>) -> Vec<(usize, usize)> {
    let mut adjacent_symbols: Vec<(usize, usize)> = Vec::new();

    for (x2, y2) in symbols {
        let distance = ((x2 as i32) - (x as i32)).abs().max(((y2 as i32) - (y as i32)).abs());
        if distance == 1 {
            adjacent_symbols.push((x2, y2));
        }
    }

    adjacent_symbols
}
