use std::fs;
use clap::Parser;
use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

#[derive(Debug, Eq)]
struct Hand {
    cards: [char; 5],
    card_values: [u8; 5],
    hand_type: u8,
    bet: u32,
}

impl Ord for Hand {
    fn cmp(&self, other: &Hand) -> Ordering {
        if self.hand_type == other.hand_type {
            for i in 0..self.cards.len() {
                if self.card_values[i] != other.card_values[i] {
                    return self.card_values[i].cmp(&other.card_values[i]);
                }
            }
        }
        self.hand_type.cmp(&other.hand_type)
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Hand) -> Option<Ordering> {
        if self.hand_type == other.hand_type {
            for i in 0..self.cards.len() {
                if self.card_values[i] != other.card_values[i] {
                    return Some(self.card_values[i].cmp(&other.card_values[i]));
                }
            }
        }
        Some(self.cmp(other))
    }
}

impl PartialEq for Hand {
    fn eq(&self, other: &Hand) -> bool {
        self.hand_type == other.hand_type
    }
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let mut hands: Vec<Hand> = Vec::new();

    // part 1 values
    // let card_values = HashMap::from([
    //     ('A', 14),
    //     ('K', 13),
    //     ('Q', 12),
    //     ('J', 11),
    //     ('T', 10),
    //     ('9', 9),
    //     ('8', 8),
    //     ('7', 7),
    //     ('6', 6),
    //     ('5', 5),
    //     ('4', 4),
    //     ('3', 3),
    //     ('2', 2),
    // ]);

    // part 2 values
    let card_values = HashMap::from([
        ('A', 14),
        ('K', 13),
        ('Q', 12),
        ('J', 2),
        ('T', 11),
        ('9', 10),
        ('8', 9),
        ('7', 8),
        ('6', 7),
        ('5', 6),
        ('4', 5),
        ('3', 4),
        ('2', 3),
    ]);

    for line in data.lines() {
        let split: Vec<_> = line.split(' ').collect();
        let cards: Vec<_> = split[0].chars().collect();

        let mut hand = Hand {
            cards: [cards[0], cards[1], cards[2], cards[3], cards[4]],
            card_values: [
                card_values[&cards[0]],
                card_values[&cards[1]],
                card_values[&cards[2]],
                card_values[&cards[3]],
                card_values[&cards[4]],
            ],
            hand_type: 0,
            bet: split[1].parse().unwrap(),
        };

        hand.hand_type = get_hand_type(hand.cards);
        hands.push(hand);
    }

    hands.sort();

    let mut total = 0;
    for i in 0..hands.len() {
        total = total + hands[i].bet * ((i as u32) + 1);
        // println!("{:?}", hands[i]);
    }
    println!("{}", total);
}

fn get_hand_type(cards: [char; 5]) -> u8 {
    let mut card_count: [u32; 15] = [0; 15];
    let mut jack_count = 0;

    for c in cards {
        if c.is_digit(10) {
            let card_index = c.to_digit(10).unwrap() as usize;
            card_count[card_index] = card_count[card_index] + 1;
        } else {
            match c {
                'T' => {
                    card_count[10] = card_count[10] + 1;
                }
                'J' => {
                    // part 1
                    // card_count[11] = card_count[11] + 1;
                    // part 2
                    jack_count = jack_count + 1;
                }
                'Q' => {
                    card_count[12] = card_count[12] + 1;
                }
                'K' => {
                    card_count[13] = card_count[13] + 1;
                }
                'A' => {
                    card_count[14] = card_count[14] + 1;
                }
                _ => todo!(),
            }
        }
    }

    let mut highest_count = 0;
    let mut has_trips = false;
    let mut has_pair = false;

    for count in card_count {
        if count == 5 {
            return 6;
        } else if count == 4 {
            highest_count = 5;
            break;
        } else if count == 3 {
            if has_pair {
                highest_count = 4;
                break;
            }
            has_trips = true;
            highest_count = 3;
        } else if count == 2 {
            if has_trips {
                highest_count = 4;
                break;
            } else if has_pair {
                highest_count = 2;
                break;
            }
            has_pair = true;
            highest_count = 1;
        }
    }

    if highest_count == 0 {
        match jack_count {
            0 => {
                return highest_count;
            }
            1 => {
                return 1;
            }
            2 => {
                return 3;
            }
            3 => {
                return 5;
            }
            4 => {
                return 6;
            }
            5 => {
                return 6;
            }
            _ => todo!(),
        }
    } else if highest_count == 1 {
        match jack_count {
            0 => {
                return highest_count;
            }
            1 => {
                return 3;
            }
            2 => {
                return 5;
            }
            3 => {
                return 6;
            }
            _ => todo!(),
        }
    } else if highest_count == 2 {
        match jack_count {
            0 => {
                return highest_count;
            }
            1 => {
                return 4;
            }
            _ => todo!(),
        }
    } else if highest_count == 3 {
        match jack_count {
            0 => {
                return highest_count;
            }
            1 => {
                return 5;
            }
            2 => {
                return 6;
            }
            _ => todo!(),
        }
    } else if highest_count == 5 {
        match jack_count {
            0 => {
                return highest_count;
            }
            1 => {
                return 6;
            }
            _ => todo!(),
        }
    }

    highest_count
}
