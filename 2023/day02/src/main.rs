use clap::Parser;
use std::fs;
use regex::Regex;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let red_max = 12;
    let green_max = 13;
    let blue_max = 14;
    let mut games_possible_sum = 0;
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let re_line = Regex::new(r"(?:Game\s*(?P<game_id>-?\d+)+):(?P<draws>.*)").unwrap();
    let re_colors = Regex::new(
        r"(?P<blue>\d*)(?:\s*blue)|(?P<green>\d*)(?:\s*green)|(?P<red>\d*)(?:\s*red)"
    ).unwrap();

    for line in data.lines() {
        let Some(game) = re_line.captures(line) else {
            return;
        };

        let mut game_possible = true;

        for (color, [count]) in re_colors.captures_iter(&game["draws"]).map(|c| c.extract()) {
            let x = count.parse::<u8>().unwrap();
            if color.contains("red") && x > red_max {
                game_possible = false;
                break;
            } else if color.contains("green") && x > green_max {
                game_possible = false;
                break;
            } else if color.contains("blue") && x > blue_max {
                game_possible = false;
                break;
            }
        }

        if game_possible {
            games_possible_sum = games_possible_sum + game["game_id"].parse::<i32>().unwrap();
        }
    }

    println!("game possible sum: {}", games_possible_sum);
}
