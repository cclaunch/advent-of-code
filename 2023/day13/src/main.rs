use std::fs;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    let notes: Vec<_> = data.split("\n\n").collect();

    'outer: for note in notes {
        let mut shift = 0;
        for line in note.lines() {
            let num = get_num(line.to_string());
            println!("num: {}", num);
            if !is_palindrome(num << shift) {
                shift = shift + 1;
                if shift > (line.len() / 2).try_into().unwrap() {
                    break;
                }
                continue;
            } else {
                println!("{} passed!", line);
            }
        }
        println!("-------");
    }
}

fn get_num(line: String) -> i32 {
    i32::from_str_radix(&line.replace("#", "1").replace(".", "0"), 2).unwrap()
}

fn is_palindrome(x: i32) -> bool {
    let (mut rev, mut org) = (0, x);

    while org > 0 {
        rev = rev * 10 + (org % 10);
        org /= 10;
    }

    rev == x
}
