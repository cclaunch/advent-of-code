use clap::Parser;
use std::fs;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let mut total = 0;

    for line in data.lines() {
        let nums: Vec<i32> = line
            .trim()
            .split(' ')
            .filter(|x| !x.is_empty())
            .map(|x| x.parse().unwrap())
            .collect();

        let mut rows: Vec<Vec<i32>> = Vec::new();
        rows.push(nums);

        loop {
            let row = get_next_row(rows.last().unwrap());
            rows.push(row);
            let sum: i32 = rows.last().unwrap().iter().sum();
            if sum == 0 {
                break;
            }
        }

        let mut first = true;
        let mut value = 0;

        // part 1
        // for row in rows.iter().rev() {
        //     if first {
        //         value = *row.last().unwrap();
        //         first = false;
        //     } else {
        //         value = value + *row.last().unwrap();
        //     }
        // }

        // part 2
        for row in rows.iter().rev() {
            if first {
                value = row[0];
                first = false;
            } else {
                value = row[0] - value;
            }
        }

        total = total + value;
    }

    println!("{}", total);
}

fn get_next_row(nums: &Vec<i32>) -> Vec<i32> {
    let mut ret = vec![0; nums.len() - 1];
    for i in 0..nums.len() - 1 {
        ret[i] = nums[i + 1] - nums[i];
    }

    ret
}
