use std::fs;
use clap::Parser;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    for line in data.lines() {
        let split: Vec<_> = line.split_whitespace().collect();

        let record_split = split[0];
        let combos_split = split[1];

        let record: Vec<char> = record_split.chars().collect();
        let combos: Vec<usize> = combos_split
            .split(',')
            .map(|x| x.parse().unwrap())
            .collect();

        get_arrangements(record, combos);

        //     for letter in letters.iter() {
        //         if *letter == '#' {
        //             println!("{}, {}", x, y);
        //             grid[x][y] = 1;
        //         } else if *letter == '?' {
        //             grid[x][y] = 2;
        //         }
    }
}

fn get_arrangements(record: Vec<char>, combos: Vec<usize>) -> u32 {
    println!("{:?} -> {:?}", record, combos);
    for i in 0..record.len() {
        // println!("{}", record[i]);
    }
    0
}

// ?###???????? 3,2,1

// .###.##.#... = 0x768 = 1896
// .###.##..#.. = 0x764 = 1892
// .###.##...#. = 0x762 = 1890
// .###.##....# = 0x761 = 1889
// .###..##.#.. = 0x734 = 1844
// .###..##..#. = 0x732 = 1842
// .###..##...# = 0x731 = 1841
// .###...##.#. = 0x71A = 1818
// .###...##..# = 0x719 = 1817
// .###....##.# = 0x70D = 1805
