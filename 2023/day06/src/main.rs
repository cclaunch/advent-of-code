fn main() {
    // sample input
    // let total = race(7, 9) * race(15, 40) * race(30, 200);

    // part 1 input
    // let total = race(61, 643) * race(70, 1184) * race(90, 1362) * race(66, 1041);

    // part 2 sample input
    // let total = race(71530, 940200);

    // part 2 input
    let total = race(61709066, 643118413621041);
    println!("{}", total);
}

fn race(time: u64, distance: u64) -> u64 {
    let mut ways_to_win = 0;
    for s in 1..time - 1 {
        if s * (time - s) > distance {
            ways_to_win = ways_to_win + 1;
        }
    }

    ways_to_win
}
