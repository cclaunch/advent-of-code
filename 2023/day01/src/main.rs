use std::fs::File;
use std::io::{ self, BufRead };
use std::path::Path;

fn main() {
    if let Ok(lines) = read_lines("./input.txt") {
        let mut total: u32 = 0;
        let mut line_number = 1;
        for line in lines {
            if let Ok(input_string) = line {
                let letters: Vec<char> = input_string.chars().collect();

                let mut number: u32 = 0;
                let mut last_digit = 0;
                let mut first_digit = true;
                let mut second_digit_found = false;
                let mut word = String::new();

                for letter in letters.iter() {
                    if letter.is_digit(10) {
                        if first_digit {
                            first_digit = false;
                            number = 10 * letter.to_digit(10).unwrap();
                        } else {
                            second_digit_found = true;
                            last_digit = letter.to_digit(10).unwrap();
                        }
                        word.clear();
                    } else {
                        word.push(*letter);
                        if word.contains("one") {
                            if first_digit {
                                first_digit = false;
                                number = 10;
                            } else {
                                second_digit_found = true;
                                last_digit = 1;
                            }
                            word = "e".to_string();
                        } else if word.contains("two") {
                            if first_digit {
                                first_digit = false;
                                number = 20;
                            } else {
                                second_digit_found = true;
                                last_digit = 2;
                            }
                            word = "o".to_string();
                        } else if word.contains("three") {
                            if first_digit {
                                first_digit = false;
                                number = 30;
                            } else {
                                second_digit_found = true;
                                last_digit = 3;
                            }
                            word = "e".to_string();
                        } else if word.contains("four") {
                            if first_digit {
                                first_digit = false;
                                number = 40;
                            } else {
                                second_digit_found = true;
                                last_digit = 4;
                            }
                            word.clear();
                        } else if word.contains("five") {
                            if first_digit {
                                first_digit = false;
                                number = 50;
                            } else {
                                second_digit_found = true;
                                last_digit = 5;
                            }
                            word = "e".to_string();
                        } else if word.contains("six") {
                            if first_digit {
                                first_digit = false;
                                number = 60;
                            } else {
                                second_digit_found = true;
                                last_digit = 6;
                            }
                            word.clear();
                        } else if word.contains("seven") {
                            if first_digit {
                                first_digit = false;
                                number = 70;
                            } else {
                                second_digit_found = true;
                                last_digit = 7;
                            }
                            word = "n".to_string();
                        } else if word.contains("eight") {
                            if first_digit {
                                first_digit = false;
                                number = 80;
                            } else {
                                second_digit_found = true;
                                last_digit = 8;
                            }
                            word = "t".to_string();
                        } else if word.contains("nine") {
                            if first_digit {
                                first_digit = false;
                                number = 90;
                            } else {
                                second_digit_found = true;
                                last_digit = 9;
                            }
                            word = "e".to_string();
                        }
                    }
                    println!("{}", word);
                }

                if second_digit_found {
                    number = number + last_digit;
                } else {
                    number = number + number / 10;
                }
                println!("Line: {}: Number: {}", line_number, number);
                line_number = line_number + 1;

                total = total + number;
            }
        }

        println!("Total: {}", total);
    }
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>> where P: AsRef<Path> {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
