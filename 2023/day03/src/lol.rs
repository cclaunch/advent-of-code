use std::fs;
use clap::Parser;
use regex::Regex;

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

#[derive(Clone, Debug)]
struct PartNumber {
    start: usize,
    end: usize,
    num: u32,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let re_line = Regex::new(r"(\d+)").unwrap();
    let re_symbols = Regex::new(r"[^\d.\n]").unwrap();
    let mut prev_symbols: Vec<u8> = Vec::new();
    let mut symbols: Vec<u8> = Vec::new();
    let mut prev_nums: Vec<PartNumber> = Vec::new();
    let mut nums: Vec<PartNumber> = Vec::new();
    let mut line_num = 0;

    for line in data.lines() {
        println!("line {}", line_num);
        if prev_symbols.len() == 0 {
            prev_symbols.resize(line.len(), 0);
        }
        if symbols.len() == 0 {
            symbols.resize(line.len(), 0);
        }
        for caps in re_symbols.captures_iter(line) {
            let cap = caps.get(0).unwrap();
            symbols[cap.start()] = 1;
        }
        for caps in re_line.captures_iter(line) {
            let cap = caps.get(0).unwrap();
            let pn = PartNumber {
                start: cap.start(),
                end: cap.end(),
                num: cap.as_str().parse::<u32>().unwrap(),
            };
            nums.push(pn);
        }

        for num in nums {
            if 
        }
        for prev_num in prev_nums {
            println!("prev_num: {:?}", prev_num);
        }

        prev_nums = nums.clone();
        nums.clear();
        line_num = line_num + 1;
    }
}
