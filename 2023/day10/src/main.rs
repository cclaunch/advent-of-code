use std::fs;
use clap::Parser;
use image::{ RgbImage, Rgb };

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

// the elegant solution I bailed on

// enum Direction {
//     North,
//     South,
//     East,
//     West,
// }

// struct Tile {
//     x: usize,
//     y: usize,
//     north: Option<Box<Tile>>,
//     south: Option<Box<Tile>>,
//     east: Option<Box<Tile>>,
//     west: Option<Box<Tile>>,
// }

// impl Tile {
//     fn new() -> Tile {
//         Tile {
//             x: 0,
//             y: 0,
//             north: None,
//             south: None,
//             east: None,
//             west: None,
//         }
//     }

//     fn push(&mut self, x: usize, y: usize, direction: Direction) {
//         let tile = Box::new(Tile {
//             x: x,
//             y: y,
//             north: None,
//             south: None,
//             east: None,
//             west: None,
//         });

//         match direction {
//             Direction::North => {
//                 let ptr: NonNull<Tile> = Box::leak(tile).into();
//                 self.north = Some(ptr);
//             }
//             Direction::South => {
//                 let ptr: NonNull<Tile> = Box::leak(tile).into();
//                 self.south = Some(ptr);
//             }
//             Direction::East => {
//                 let ptr: NonNull<Tile> = Box::leak(tile).into();
//                 self.east = Some(ptr);
//             }
//             Direction::West => {
//                 let ptr: NonNull<Tile> = Box::leak(tile).into();
//                 self.west = Some(ptr);
//             }
//         }
//     }
// }

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");

    let width = data.find("\n").unwrap();
    let height =
        (data.len() -
            data
                .as_bytes()
                .iter()
                .filter(|&&c| c == b'\n')
                .count()) /
        width;
    let mut grid = vec![vec![0 as i8; width]; height];

    let mut x = 0;
    let mut y = 0;
    let mut start_x = 0;
    let mut start_y = 0;

    for line in data.lines() {
        let letters: Vec<char> = line.chars().collect();

        for letter in letters.iter() {
            if *letter == '|' {
                grid[x][y] = 1;
            } else if *letter == '-' {
                grid[x][y] = 2;
            } else if *letter == 'L' {
                grid[x][y] = 3;
            } else if *letter == 'J' {
                grid[x][y] = 4;
            } else if *letter == '7' {
                grid[x][y] = 5;
            } else if *letter == 'F' {
                grid[x][y] = 6;
            } else if *letter == 'S' {
                grid[x][y] = 7;
                start_x = x;
                start_y = y;
            }
            y = y + 1;
        }
        x = x + 1;
        y = 0;
    }

    // println!("{}, {}", start_x, start_y);
    // println!("{:?}", grid);

    x = start_x;
    y = start_y;
    let mut first = true;
    let mut steps = 0;
    // 0 = left, 1 = up, 2 = right, 3 = down
    let mut last_move = -1;
    let mut path_points = vec![vec![0 as usize; width]; height];

    loop {
        let current = grid[x][y];
        path_points[x][y] = 1;
        println!("{}, {}", x, y);
        if current == 7 && !first {
            println!("steps: {}", steps);
            break;
        }

        first = false;
        // is there anything left?
        if y > 0 && last_move != 2 {
            // check left
            let left = grid[x][y - 1];
            // println!("left: {}", left);
            if left > 0 {
                if current == 2 || current == 4 || current == 5 || current == 7 {
                    if left == 2 || left == 3 || left == 6 || left == 7 {
                        y = y - 1;
                        last_move = 0;
                        steps = steps + 1;
                        // println!("left");
                        continue;
                    }
                }
            }
        }
        // is there anything up?
        if x > 0 && last_move != 3 {
            // check up
            let up = grid[x - 1][y];
            // println!("up: {}", up);
            if up > 0 {
                if current == 1 || current == 3 || current == 4 || current == 7 {
                    if up == 1 || up == 5 || up == 6 || up == 7 {
                        x = x - 1;
                        last_move = 1;
                        steps = steps + 1;
                        // println!("up");
                        continue;
                    }
                }
            }
        }
        // is there anything right?
        if y < width - 1 && last_move != 0 {
            // check right
            let right = grid[x][y + 1];
            // println!("right: {}", right);
            if right > 0 {
                if current == 2 || current == 3 || current == 6 || current == 7 {
                    if right == 2 || right == 4 || right == 5 || right == 7 {
                        y = y + 1;
                        last_move = 2;
                        steps = steps + 1;
                        // println!("right");
                        continue;
                    }
                }
            }
        }
        // is there anything down?
        if x < height - 1 && last_move != 1 {
            // check down
            let down = grid[x + 1][y];
            // println!("down: {}", down);
            if down > 0 {
                if current == 1 || current == 5 || current == 6 || current == 7 {
                    if down == 1 || down == 3 || down == 4 || down == 7 {
                        x = x + 1;
                        last_move = 3;
                        steps = steps + 1;
                        // println!("down");
                        continue;
                    }
                }
            }
        }

        // shouldn't get here ever
        break;
    }

    let mut img = RgbImage::new(height.try_into().unwrap(), width.try_into().unwrap());

    let mut x = 0;
    let mut y = 0;
    for row in &path_points {
        x = 0;
        println!("{:?}", row);
        for _col in row {
            println!("{} {}", x, y);
            if &path_points[y][x] > &0 {
                img.put_pixel(y as u32, x as u32, Rgb([255, 255, 255]));
            }

            x = x + 1;
        }
        y = y + 1;
    }

    // println!("{:?}", path_points);
    // let mut in_path = false;
    // let mut prev_zero = false;
    // let mut total = 0;
    // let mut zero_count = 0;

    // for i in 0..height - 1 {
    //     in_path = false;
    //     zero_count = 0;
    //     for j in 0..width - 1 {
    //         if path_points[i][j] == 1 {
    //             let pixel = *img.get_pixel(i as u32, j as u32);
    //             img.put_pixel(i as u32, j as u32, pixel);
    //             in_path = true;
    //             if zero_count > 0 {
    //                 total = total + zero_count;
    //                 zero_count = 0;
    //             }
    //         } else if path_points[i][j] == 0 {
    //             if in_path {
    //                 zero_count = zero_count + 1;
    //             }
    //         }
    //     }
    //     println!("------------------{}", total);
    // }

    img.save("output.png").unwrap();
    // println!("total: {}", total);
}

// fn get_next(x: usize, y: usize, grid: &Vec<(usize, usize)>) -> (usize, usize) {
//     let next_x = 0;
//     let next_y = 0;

//     // are we back at the start position? just return back the same thing then
//     if grid[x][y] == 7 {
//         (x, y)
//     }

//     // is there anything left?
//     if x > 0 {
//         match grid[x-1][y] {
//             1|2|
//         }
//     }
// }

// 1234 too high
