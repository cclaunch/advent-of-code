use clap::Parser;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::cmp::{ max, min };

#[derive(Parser)]
struct Cli {
    #[arg(short = 'i', long = "input")]
    input: std::path::PathBuf,
}

fn main() {
    let args = Cli::parse();
    let data = fs::read_to_string(args.input).expect("unable to read file");
    let re_line = Regex::new(r"([A-Z0-9]{3})").unwrap();
    let mut map: HashMap<String, [String; 2]> = HashMap::new();
    let mut keys: Vec<String> = Vec::new();
    let mut instructions: Vec<usize> = Vec::new();
    let mut first = true;

    for line in data.lines() {
        if first {
            let letters: Vec<char> = line.chars().collect();

            for letter in letters.iter() {
                if *letter == 'L' {
                    instructions.push(0);
                } else {
                    instructions.push(1);
                }
            }
            first = false;
        } else if !line.is_empty() {
            let words: Vec<String> = re_line
                .find_iter(line)
                .map(|x| x.as_str().to_string())
                .collect();
            map.insert(words[0].clone(), [words[1].clone(), words[2].clone()]);
            keys.push(words[0].clone());
        }
    }

    // let mut ins = "AAA";
    // let mut ins_index = 0;
    // let mut step_count: u32 = 1;
    // let mut ins_key: usize;

    let mut inss: Vec<String> = Vec::new();

    for key in keys.iter() {
        if key.chars().nth(2).unwrap() == 'A' {
            inss.push(key.clone());
        }
    }

    // println!("keys: {:?}", keys);
    println!("inss: {:?}", inss);

    // let mut ins = "AAA";

    let mut step_count: u128 = 1;
    let mut ins_key;
    let mut prev_step_count = 1;
    let mut step_counts: Vec<u128> = Vec::new();

    for x in 0..inss.len() {
        let mut ins_index = 0;
        let mut ins = &inss[x];
        println!("ins = {}", ins);
        let mut z_count = 0;
        let mut current_step_count = 0;
        loop {
            ins_key = keys
                .iter()
                .position(|i| i == ins)
                .unwrap();

            ins = &map[&keys[ins_key]][instructions[ins_index]];
            // println!("insi = {}", ins);
            // println!("instruction = {}", instructions[ins_index]);
            if ins == "XXX" {
                break;
            }

            if ins.chars().nth(2).unwrap() == 'Z' {
                // z_count = z_count + 1;
                // if z_count == ins.len() {
                println!("{:?}: z: {}", ins, z_count);
                current_step_count = current_step_count + 1;
                break;
                // }
            }

            ins_index = ins_index + 1;
            if ins_index >= instructions.len() {
                ins_index = 0;
            }

            current_step_count = current_step_count + 1;
        }

        println!("current_step_count: {}", current_step_count);
        step_counts.push(current_step_count);

        // step_count = step_count + prev_step_count * current_step_count;
        // prev_step_count = current_step_count;
    }

    println!("{:?}", step_counts);

    let mut prev_lcm = lcm(step_counts[0], step_counts[1]);

    for i in 2..step_counts.len() {
        let curr_lcm = lcm(prev_lcm, step_counts[i]);
        println!("prev: {}, step: {}", prev_lcm, step_counts[i]);
        prev_lcm = curr_lcm;
    }

    println!("final: {}", prev_lcm);

    // earlier try brute force way too long
    // // let mut ins_index: Vec<usize> = Vec::with_capacity(ins.len() as usize);
    // let mut ins_key = vec![0; ins.len()];
    // let mut ins_index = 0;
    // let mut prev_z_count = 0;
    // 'outer: loop {
    //     // println!("ins_index: {}", ins_index);
    //     let mut z_count = 0;
    //     for i in 0..ins.len() {
    //         // println!("ins: {:?}", ins);
    //         // println!("keys: {:?}", keys);
    //         // println!("i: {}", i);
    //         ins_key[i] = keys
    //             .iter()
    //             .position(|x| x == &ins[i])
    //             .unwrap();

    //         ins[i] = map[&keys[ins_key[i]]][instructions[ins_index]].clone();

    //         if ins[i].chars().nth(2).unwrap() == 'Z' {
    //             z_count = z_count + 1;
    //             if z_count == ins.len() {
    //                 println!("{:?}: z: {}", ins, z_count);
    //                 break 'outer;
    //             }
    //         }

    //         // println!("ins: {:?}", ins);
    //     }

    //     ins_index = ins_index + 1;
    //     if ins_index >= instructions.len() {
    //         ins_index = 0;
    //     }

    //     if z_count > prev_z_count {
    //         println!("{:?}: z: {}", ins, z_count);
    //         println!("{}", step_count);
    //         prev_z_count = z_count;
    //     }
    //     step_count = step_count + 1;
    // }

    println!("{}", step_count);
}

fn gcd(a: u128, b: u128) -> u128 {
    match ((a, b), (a & 1, b & 1)) {
        ((x, y), _) if x == y => y,
        ((0, x), _) | ((x, 0), _) => x,
        ((x, y), (0, 1)) | ((y, x), (1, 0)) => gcd(x >> 1, y),
        ((x, y), (0, 0)) => gcd(x >> 1, y >> 1) << 1,
        ((x, y), (1, 1)) => {
            let (x, y) = (min(x, y), max(x, y));
            gcd((y - x) >> 1, x)
        }
        _ => unreachable!(),
    }
}

fn lcm(a: u128, b: u128) -> u128 {
    (a * b) / gcd(a, b)
}
