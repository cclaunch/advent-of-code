Notes / TODOs
=============

Just a list of random notes, things I'd do differently next year, etc.

2018
----

* The "one file to rule them all" for each language strategy did indeed keep the CI easier, but ended up getting huge and confusing.
* Can always go back later and add the rank/total values
